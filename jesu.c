#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>

int n, m, nhilos=0, **A, *B, *C, i, j, r, c, k, Fila=0 ;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void *multiplicar(void *param){

    
    
    int filaActual = Fila++;
    
    
    if(filaActual<m){
            for( r = 0; r < n; r++){
                C[filaActual] += A[filaActual][r] * B[r]; 
            }
    }  
  
    if (filaActual<m) {
        multiplicar(NULL);
    }
    

    return NULL;
}
    

int main(int argc, char const *argv[])
{   
    

    
    printf("Ingrese Tamaño de n\n");
    scanf("%d",&n);
    printf("Ingrese Tamaño de m\n");
    scanf("%d",&m);

    printf("Ingrese tamaño de hilos\n");
    scanf("%d",&nhilos);

    A= (int **) malloc(m*sizeof(int *));
    
    
    
    
    for( i = 0; i <m ; i++)
    {
        A[i]= (int *) malloc(n*sizeof(int));
    }

    B= (int *) malloc(n*sizeof(int));
    C= (int *) malloc(n*sizeof(int ));
    
    printf("Matriz A\n");
    printf("\n");
    int aux=0;
    for( r=0; r<m; r++){
            B[r] = r+1;
        for( c = 0; c < n; c++){
            A[r][c] = aux++;
             printf("%d ", A[r][c]);
        }   
        printf("\n");        
    }
    printf("\n");   
    printf("Matriz B\n");
    printf("\n");
    for( r=0; r<n; r++){
        printf("%d\n", B[r]);      
    }

    
    pthread_t hilos[nhilos];
   
    for( i = 0; i < nhilos; i++){ 
        
        pthread_create(&hilos[i], NULL, multiplicar, NULL);
        
    }

    for( i = 0; i < nhilos; i++) pthread_join(hilos[i], NULL);
    
    printf("Matriz Resultante\n");
    printf("\n");
    for( r=0; r<m; r++){
        
            printf("[%d] ", C[r]);
             
    }

    return 0;
}