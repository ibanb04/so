#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include <wait.h>


int main(int argc, char const *argv[])
{
    int i;
    __pid_t childs[2], padre = getpid();
    wait(NULL);
    for (i=0;i<2;i++)
    {
        childs[i] = fork();
        if (childs[i] == 0){
            break;
        }
    }
    if(padre==getpid()){
        char b[500];
        sprintf(b,"pstree -lp %d",getpid());
        system(b);
    }else{
        sleep(1);
    }
    return 0;
}
