#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<wait.h>

int main(int argc, char const *argv[])
{
    __pid_t childs[8], padre = getpid();
    int i;
    wait(NULL);
    
    for( i = 0; i < 3; i++)
    {
        childs[i] = fork();
        if(childs[i] == 0){
        if(i == 0 || i == 2){
            wait(NULL);
            childs[i] = fork();            
            if (childs[i]==0) {
                break;
            }
        }
        
        if (i == 1) {
            wait(NULL);
            childs[i] = fork();
            if(childs[i] == 0){
                wait(NULL);
                for( i = 0; i < 2; i++)
                {
                    childs[i] = fork();
                    if(childs[i] == 0){
                        break;
                    }
                }
                
            break;
            }
        }
        
        break;
        }
    }
    

    if(padre==getpid()){
        char b[500];
        sprintf(b,"pstree -lp %d",getpid());
        system(b);
    }else{
        sleep(1);
    }
    return 0;
}
