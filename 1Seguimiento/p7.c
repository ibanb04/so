#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<wait.h>

int main(int argc, char const *argv[])
{
    int i, j = -1, k=-1, n, nivel = 0;
    pid_t padre = getpid();
    printf("ingrese el tamaño \n");
    scanf("%d", &n);
    pid_t *childs =(pid_t*)calloc(n,sizeof(pid_t));

    for( i = 0; i < n; i++)
    {
        childs[i] = fork();        
        if (childs[i] == 0) {
            nivel++;
            if(i > 0){               
                for (k=0; k<i; k++){
                    
                    for( j = 0; j < nivel+1; j++)
                    {
                        childs[j] = fork();
                        if (childs[j] == 0) {
                            nivel++;
                            break;
                        }
                        if (childs[i] > 0) break;
                    }
                    
                }
                
            }
            break;
        }
        
    }
    if(padre==getpid()){
        char b[500];
        sprintf(b,"pstree -lp %d",getpid());
        system(b);
       
    }else{
        sleep(3);
    } 
    printf("%d %d \n", getpid(), nivel);
    return 0;
}
