#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<wait.h>

int main(int argc, char const *argv[])
{
    int child,n, padre = getpid();
    int i;
    printf("ingrese el numero de hijos \n");
    scanf("%d", &n);
    
    for(i=0; i<n ;i++)
        if((child = fork())) break;  //si es diferente de cero dispara break           
    
    //printf("%d\n",getpid());

    if(padre==getpid()){
        sleep(2);
        char b[500];
        sprintf(b,"pstree -lp %d",getpid());
        system(b);
    }else{
        sleep(5);
    }
    return 0;
}