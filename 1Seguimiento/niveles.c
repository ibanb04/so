#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<wait.h>

int main(int argc, char const *argv[])
{
    int crearProcesos(int n){
        if(n > 0){
        pid_t childs[3];
            for(int i = 0; i < 3; i++)
            {
                if(!(childs[i] = fork())){
                    if( i == 1 && (n-1)!= 0){
                        if(!(childs[i] = fork())){
                            crearProcesos(n-1);
                            break;
                        }
                    }
                    break;
                }
            }    
        }        
    }

    int n;
    pid_t padre = getpid();
    printf("ingrese el valor de n\n");
    scanf("%d", &n);
    crearProcesos(n);
    if(padre==getpid()){
        char b[500];
        sprintf(b,"pstree -lp %d",getpid());
        system(b);
       
    }else{
        sleep(3);
    }     
    return 0;
}

