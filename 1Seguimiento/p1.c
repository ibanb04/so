#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<wait.h>

int main(int argc, char const *argv[])
{
    pid_t childs[3], padre = getpid();
    int i;

    for( i = 0; i < 2; i++)
    {
        childs[i] = fork();
        if(childs[i] == 0){
            if(i == 1){
                childs[i] = fork();
                if(childs[i] == 0){
                    break;
                }
            }
            break;
        }
    }
    
    if(padre==getpid()){
        char b[500];
        sprintf(b,"pstree -lp %d",getpid());
        system(b);

        for( i = 0; i < 2; i++) wait(NULL);
    }else{
        sleep(2);
        if(getppid() == padre && i == 1) wait(NULL);
    }

    return 0;
}
