#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<wait.h>

int main(int argc, char const *argv[])
{
    int n,i,j = -1,k = -1, padre = getpid();
    printf("ingrese el numero de procesos\n");
    scanf("%d",&n);
    pid_t *childs =(pid_t*)calloc(n,sizeof(pid_t));
    
    for( i = 0; i < n; i++)
    {        
        if (!(childs[i] = fork())) {
            
            if (i == n-1) {
                
                for( j = 0; j < n/2; j++)
                {
                    if (!(childs[j] = fork())){
                        
                        if ( j == (n/2)-1) {
                            
                            for( k = 0; k < n/4; k++)
                            {
                                if(!(childs[k] = fork())){
                                    
                                    break;
                                }
                            }
                            
                        }
                        
                        break;
                    }
                }
                
            }
            
            break;
        }  
    }  
    if(padre==getpid()){
        char b[500];
        sprintf(b,"pstree -lp %d",getpid());
        system(b);
        for( i = 0; i < n; i++) wait(NULL);
    }else{
        sleep(3);
    } 
   // printf("%d %d \n", getpid(), i);
    return 0;
}