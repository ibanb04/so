#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<wait.h>

int main(int argc, char const *argv[])
{
    int i;
    pid_t padre = getpid();
    for(i=0;i < 2; i++){
        fork();
        if(i%2 == 0){
            fork();
        }
    }
    if(padre==getpid()){
        char b[500];
        sprintf(b,"pstree -lp %d",getpid());
        system(b);
       
    }else{
        sleep(3);
    } 
    //printf("%d %d \n", getpid(), nivel);
    return 0;
}
