#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>	
#include <time.h>
struct timespec tic, toc;
double Elapsed;
#define MAX_SIZE 100000
int NHijos = 20;
double *respuesta, *a, *b;


void *rountime(void*param);
int main(){
    clock_gettime(CLOCK_REALTIME, &tic);
    double escalar = (double) 1/MAX_SIZE;
    printf("escalar: %.16f",escalar);
    a = (double*) calloc (MAX_SIZE, sizeof(double));
    b = (double*) calloc (MAX_SIZE, sizeof(double));
    respuesta = (double*) calloc (NHijos, sizeof(double));
    //randomize(time(NULL));
    srand(time(NULL));
    for(int i=0; i<10; i++){
        a[i] = rand();
        b[i] = rand();
    }
    //IMPRESION
    /*
    printf("Vector1: ");
    for(int i=0; i<MAX_SIZE; i++)
    printf("%.1f\t",a[i]);
    printf("\n");
    printf("Vector2: ");
    for(int i=0; i<MAX_SIZE; i++)
    printf("%.1f\t",b[i]);
    printf("\n");
    */
    pthread_t pidhilos[NHijos];
    for(int j=0; j<NHijos; j++){
        int*aux = (int*) malloc(sizeof(int));
        aux[0] = j;
        pthread_create(&pidhilos[j],NULL,rountime,(void*)aux);
    }

    for(int j=0; j<NHijos; j++){
        pthread_join(pidhilos[j],NULL);
    }
    
    printf("\n");
    double suma = 0;
    for(int j=0; j<NHijos; j++){
        suma += respuesta[j];
    }
    printf("Suma: %.1f",suma);
    printf("\n");
    clock_gettime(CLOCK_REALTIME, &toc);
    Elapsed = (toc.tv_sec-tic.tv_sec)+((toc.tv_nsec-tic.tv_nsec)/(double)1E9);
    printf("Tiempo: %.61f\n",Elapsed);

    return 0;
}
void *rountime(void*param){
    int* indice = (int*) param;
    double suma = 0;
    int partes = MAX_SIZE / NHijos;
    for(int i=(int)(*indice)*partes; i<(int)(*indice+1)*partes; i++){
        suma += (a[i] - b[i]) * (a[i] - b[i]);
    }

    double escalar =  1/(double)MAX_SIZE;

    suma = (double) suma * escalar;
    respuesta[(int)(*indice)] = (double)suma;
}
