#include <unistd.h>
#include <wait.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

void create_index(void **m, int rows, int cols, size_t sizeElement){
    int i;
    size_t sizeRow = cols * sizeElement;
    m[0] = m + rows;
      
    for( i = 1; i < rows; i++)
    {
        m[i] = (m[i-1] + sizeRow);
    }
}
unsigned int sizeof_dm(int rows, int cols, size_t sizeElement){
    size_t size = rows * sizeof(void *);// indexSize
    size += (cols * rows *sizeElement);// datasize
    return size;
}

int main()
{
    int n;
    printf("digite N:\n");
    scanf("%d",&n);

    pid_t padre = getpid();
    int *turno;
    int **A = NULL, **B = NULL, **result = NULL, shmid1, shmid2, shmid3, i;
    size_t sizeMatrix = sizeof_dm(n,n,sizeof(int));

    shmid1 = shmget(IPC_PRIVATE, sizeMatrix, IPC_CREAT|0600);
    shmid2 = shmget(IPC_PRIVATE, sizeMatrix, IPC_CREAT|0600);
    shmid3 = shmget(IPC_PRIVATE, sizeMatrix, IPC_CREAT|0600);
    int shmId_t = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT|0600);
    
    turno = shmat(shmId_t,NULL,0);
    A =  shmat(shmid1,NULL,0);
    B =  shmat(shmid2,NULL,0);
    result =  shmat(shmid3,NULL,0);

    create_index((void*)A, n, n, sizeof(int));
    create_index((void*)B, n, n, sizeof(int));
    create_index((void*)result, n, n, sizeof(int));
    *turno = 0;
    double nProcesos = round((n+1)/2);
    printf("el numero de procesos es: %.f\n",nProcesos);
    for( i = 0; i < nProcesos; i++) if(!(fork()))break;

    if( padre == getpid()){
        int aux = 0;
        for(int r = 0; r < n; r++){
            for(int c = 0; c < n; c++){
                A[r][c] = aux;
                B[r][c] = aux;
                aux++;
            }
        }
        printf("las matrices A y B son:\n");
        printf("A\n");
        for(int r = 0; r < n; r++){
            for(int c = 0; c < n; c++){
                printf("[%d]\t",A[r][c]);
            }
            printf("\n");
        }
        printf("B\n");
        for(int r = 0; r < n; r++){
            for(int c = 0; c < n; c++){
                printf("[%d]\t",B[r][c]);
            }
            printf("\n");
        }
        printf("\n");
        *turno = 1;
        for( i = 0; i < nProcesos; i++) wait(NULL);
        printf("soy el padre y la matriz resultante es\n"); 
        for(int r = 0; r < n; r++)
        {
            for(int a = 0; a < n; a++){
            printf("[%d] \t",result[r][a]);
            }
        printf("\n");
        }
        printf("\n");

        shmdt(A);
        shmdt(B);
        shmdt(result); 
        shmctl(shmid1,IPC_RMID,0);
        shmctl(shmid2,IPC_RMID,0);
        shmctl(shmid3,IPC_RMID,0);
    }else{
        while(*turno == 0);
        int nFilas = n, nCols = n;
        for (int c=i; c<n-i; c++){
            result[i][c] = 0;
            for(int k=0;k<n;k++){
                result[i][c]=(result[i][c]+(A[i][k]*B[k][c]));
                //printf("soy el proceso %d --> %d\n",i, result[i][c]);
                if (i != nCols-i-1) {
                    result[(nFilas-1)-i][c]=(result[(nFilas-1)-i][c]+(A[(nFilas-1)-i][k]*B[k][c]));
                    //printf("[%d]soy el proceso %d --> %d\n",getpid(),i,result[(nFilas-1)-i][c]);
                }
                
            }
            
        }
        for (int f=i+1; f<(n-1)-i; f++)
        {
            result[f][i] = 0;
            for(int k=0;k<n;k++){
                result[f][i]=(result[f][i]+(A[f][k]*B[k][i]));
                result[f][(nCols-1)-i]=(result[f][(nCols-1)-i]+(A[f][k]*B[k][(nCols-1)-i]));
                //printf("soy el proceso %d --> %d\n",i, result[f][(nCols-1)-i]);
            }
        }
        
        shmdt(A);
        shmdt(B);
        shmdt(result);
    }
    
    return 0;
}
