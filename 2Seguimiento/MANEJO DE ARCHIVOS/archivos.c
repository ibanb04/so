#include <stdio.h>
#include <stdlib.h>

int main()
{
    char linea[1024];
    FILE *archivo;

    archivo = fopen("prueba.txt", "r");
    //Lee línea a línea y escribe en pantalla hasta el fin de fichero
    while(fgets(linea, 1024, (FILE*) archivo)) {
        printf(" %s ", linea);
    }
    fclose(archivo);
    return 0;
}
