#include <stdio.h>
#include <stdlib.h>

/*
	Modos de apertura: a, r, w (a+, ab, r+, rb, wb, w+, ab+)
*/

int main(){
	FILE *archivo = fopen("file.txt", "r");
	
	if(archivo){
		//Leer por formato, por caracter y por linea
		fseek(archivo, 0, SEEK_END);
		int t_buffer = ftell(archivo);
		
		fseek(archivo, 0, SEEK_SET);
		printf("%d", t_buffer);
		char c[t_buffer];
		while(!feof(archivo)){
			fgets(c, t_buffer, archivo);
			printf("%s", c);
		}
	}else{
		printf("Error de apertura!");
	}
}
