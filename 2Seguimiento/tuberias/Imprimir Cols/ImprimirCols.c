#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
    pid_t padre = getpid();
    int n, i;
    FILE *f = fopen("files.txt","r");
    if(!f)exit(-1);
    fscanf(f,"%d",&n);
    int **fd = (int **) calloc(n,sizeof(int*));
    
    for(int j = 0; j < n; j++)
    {
        fd[j] = (int *)calloc(2,sizeof(int));
    }
    
    for(int j = 0; j < n; j++) if(pipe(fd[j]) != 0) exit(-1);
    
    for( i = 0; i < n; i++) if(!fork()) break;
    
    if (padre == getpid() && i == n){
        
        for( i = 0; i < n; i++)
        {
            close(fd[i][0]);
            if(i != 0)close(fd[i][0]);
        }
        int m[n][n];
        for(int r = 0; r < n; r++)
        {
            for(int c = 0; c < n; c++)
            {
                fscanf(f,"%d",&m[r][c]);
                printf("%d\t",m[r][c]);
            }
            printf("\n");
        }
        write(fd[0][1],m,sizeof(int)*(n*n));
        close(fd[0][1]);
        fclose(f);    
    }else{
        for(int k = 0; k < n; k++){
            if(k != i)close(fd[k][0]);
            if(k != (i+1))close(fd[k][1]);
        }
        int a[n][n];
        read(fd[i][0],&a,sizeof(int)*(n*n));
        for(int j = 0; j < n; j++){
            printf("%d",a[j][i]);
        }
        printf("<-- hijo%d\n",i+1);
        write(fd[i+1][1],&a,sizeof(int)*(n*n));
        close(fd[i][0]);
        close(fd[i+1][0]);
    }
    
    return 0;
}
