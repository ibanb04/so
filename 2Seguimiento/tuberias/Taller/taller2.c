#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <wait.h>

int main(int argc, char const *argv[])
{
    pid_t childs[2], padre = getpid();
    int fd1[2];
    int fd2[2], i, j;
    pipe(fd1);
    pipe(fd2);
    
    for( i = 0; i < 2; i++)
    {
        if(!(childs[i] = fork())) break;
    }

    if(getpid() != padre){
        if(i == 0){
            close(fd1[0]);
            close(fd2[0]);
            close(fd2[1]);
            
            FILE *archivo = fopen("archivo1.txt", "r");;
            
            if (!archivo) {
                printf("no se pudo leer el archivo \n");
                exit(-1);
            }
            fseek(archivo,0,2);
            int tam1 = ftell(archivo);
            fseek(archivo,0,0);
            int n_linea = 0;// numero de caracteres por linea
            char linea[tam1];
            //Lee línea a línea y escribe en pantalla hasta el fin de fichero
            write(fd1[1],&tam1,sizeof(int));
            
            for( i = 0; i < tam1; i++)
            {
                if(linea[i] != '\n'){
                    linea[i] = getc(archivo);
                    write(fd1[1],linea[i],sizeof(int));
                }else{
                    n_linea++;
                    write(fd1[1],&n_linea,sizeof(int));
                }
            }
            
            fclose(archivo);          
            close(fd1[1]);
        }

        if (i == 1) {
            close(fd1[0]);
            close(fd1[1]);
            close(fd2[0]);
            FILE *archivo = fopen("archivo2.txt", "r");;
            
            if (!archivo) {
                printf("no se pudo leer el archivo \n");
                exit(-1);
            }
            fseek(archivo,0,2);
            int tam2 = ftell(archivo);
            fseek(archivo,0,0);

            char linea[tam2];
            int n_linea;
            //Lee línea a línea y escribe en pantalla hasta el fin de fichero
            for( i = 0; i < tam2; i++)
            {
                if(linea[i] != '\n'){
                    linea[i] = getc(archivo);    
                    write(fd1[1],linea[i],sizeof(int));
                }else{
                    n_linea++;
                    write(fd1[1],&n_linea,sizeof(int));
                }
            }
            
            fclose(archivo);                      
            close(fd2[1]);
            }
    }else{
          close(fd1[1]);  
          close(fd2[1]);
          int tam1 ;
          read(fd1[0],&tam1,sizeof(int));
          char linea1[tam1];
          
          for( i = 0; i < tam1; i++)
          {
              read(fd1[0],&linea1[i],tam1);
              printf("%c",linea1[i]);
              if(linea1[i] == '\n'){
                 
              }
              
          }          
          
          int tam2 ;
          read(fd2[0],&tam2,sizeof(int));
          char linea2[tam2];
          
          for( i = 0; i < tam2; i++)
          {
              read(fd2[0],&linea2[i],tam2);
              printf("%c",linea2[i]);
          }
          
          
         // printf(" tamaño 1:%d", tam1);
         // printf(" tamaño 2:%d", tam2);
         
         //printf("archivo 1: %s \n",linea2);

          close(fd1[0]);
          close(fd2[0]);
    }


    if(padre==getpid()){
        char b[500];
        sprintf(b,"pstree -lp %d",getpid());
        system(b);
        
        for( i = 0; i < 2; i++)
        {
            wait(NULL);
        }
        
    }else{
        sleep(1);
}

    return 0;
}
