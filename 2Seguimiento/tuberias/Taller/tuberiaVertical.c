#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>

int main()
{
    char buff[1024];
    int n,i,j = 0;
    printf("ingrese el numero de hijos : \n");
    scanf("%d", &n);
    int ntuberias = n*2;
    int fd[ntuberias][2];
    pid_t childs[n], padre = getpid();
    
    for( i = 0; i < ntuberias; i++)
    {
        if(pipe(fd[i])){
            printf("no se pudieron crear las tuberias");
            exit(-1);
        }        
    }
    
    for( i = 0; i < n; i++)
    {
        if(childs[i] = fork()) break;
    }

    if(padre != getpid()){
       
        for(int j = 0; j < ntuberias; j++){
            if(j != (i-1) || j!= (ntuberias-i)-1)close(fd[j][0]);
            if(j!=i|| j!=(ntuberias-i))close(fd[j][1]);
        }
        char msj[1024];
        read(fd[i-1][0],&msj,strlen(msj));
        printf("i:%d,%s, pid:%d\n",i,msj,getpid());
        write(fd[i][1],&msj,strlen(msj));
        read(fd[(ntuberias-i)-1][0],&msj,strlen(msj));
        printf("i:%d,%s, pid:%d\n",i,msj,getpid());
        write(fd[ntuberias-i][1],&msj,strlen(msj));
        close(fd[i-1][0]);
        close(fd[(ntuberias-i)-1][0]);
        close(fd[i][1]);
        close(fd[ntuberias-i][1]);

    }else{
        for(int a = 0; a < ntuberias; a++){
            if(a != (ntuberias-1))close(fd[a][0]);
            if(a!=i)close(fd[a][1]);
        }
        char msj[1024];
        printf("ingrese el mensaje: \n");
        scanf("%s",&msj);
        write(fd[i][1],&msj, strlen(msj));
        read(fd[ntuberias-1][0],&msj,strlen(msj));
        close(fd[i][1]);
        close(fd[ntuberias-1][1]);
    }

    if(padre==getpid()){   
        char b[500];
        sprintf(b,"pstree -lp %d",getpid());
        system(b);
        for( int a = 0; a < n; a++) wait(NULL);
    }else{
        sleep(2);  
    }
    return 0;
}
