#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <wait.h>

int main()
{
    char buffer[1024];
    pid_t padre = getpid();
    int n,i,m, o;
    printf("ingrese el numero de hijos:\n");
    scanf("%d",&n);
    
    int fd1[n][2], fd2[n][2];

    for(i = 0; i < n; i++) {
        if(pipe(fd1[i])) exit(-1);
        if(pipe(fd2[i])) exit(-1);
    }
    
    for( i = 0; i < n; i++) if(fork()) break;
    
    if (getpid()!=padre) {
        
        if(i == n){// ultimo hijo
            for( int k = 0; k < n; k++)
            {
               if(k != (i-1))close(fd1[k][0]);
               close(fd1[k][1]);
               close(fd2[k][0]);
               if(k != (n-1))close(fd2[k][1]);  
            }
            o = read(fd1[i-1][0],buffer,sizeof(buffer));
            buffer[o] = '\0';
            printf("i:%d\tpid:%d\t%s \n",i,getpid(),buffer);
            write(fd2[i-1][1],buffer,strlen(buffer));
            close(fd1[i-1][0]);
            close(fd2[i-1][1]);
        }else{//hijos intermedios
            for( int k = 0; k < n; k++)
            {
                if(k != (i-1))close(fd1[k][0]);
                if(k != i)close(fd1[k][1]);
                if(k != i)close(fd2[k][0]);
                if(k != (i-1))close(fd2[k][1]);
            }
            m = read(fd1[i-1][0],buffer,sizeof(buffer));
            buffer[m] = '\0';
            printf("i:%d\tpid:%d\t%s \n",i,getpid(),buffer);
            write(fd1[i][1],buffer,strlen(buffer));
            m = read(fd2[i][0],buffer,sizeof(buffer));
            buffer[m] = '\0';
            printf("i:%d\tpid:%d\t%s \n",i,getpid(),buffer);
            write(fd2[i-1][1],buffer,strlen(buffer));
            close(fd1[i-1][0]);
            close(fd1[i][1]);
            close(fd2[i][0]);
            close(fd2[i-1][1]);
        }
        
    }else{
        //printf("hola soy i=%d\n",i);
        for( int k = 0; k < n; k++)
        {
            close(fd1[k][0]);
            if(k != i)close(fd1[k][1]);
            if(k != i)close(fd2[k][0]);
            close(fd2[k][1]);
        }
        char info [1024];
        printf("ingrese el texto\n");
        scanf("%s",&info);
        strcpy(buffer,info);
        write(fd1[i][1],buffer,strlen(buffer));
        m = read(fd2[i][0],buffer,sizeof(buffer));
        buffer[m] = '\0';
        printf("i:%d\tpid:%d\t%s \n",i,getpid(),buffer);
        close(fd1[i][1]);
        close(fd2[i][1]);
    }
    
    if(padre==getpid()){   
        char b[500];
        sprintf(b,"pstree -lp %d",getpid());
        system(b);
        for( int a = 0; a < n; a++) wait(NULL);
    }else{
        sleep(2);  
    }
    return 0;
}