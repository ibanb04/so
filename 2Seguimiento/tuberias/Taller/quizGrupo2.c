#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <wait.h>

int main()
{
    char buffer[1024];
    //char buff[1024];
    pid_t childs[3], padre = getpid();
    int i, n, nimp, j = -1, k = -1, nivel= -1;
    printf("ingrese el numero de impresiones\n");
    scanf("%d",&nimp);
    int fd[11][2];
    for( i = 0; i < 11; i++)
    {
        if(pipe(fd[i])){
            printf("no se pudieron crear las tuberias");
            exit(-1);
        }        
    }
    for( i = 0; i < 3; i++)
    {
        if (!(childs[i] = fork())) {
            nivel++;
            if( i == 1){
                for( j = 0; j < 2; j++)
                {
                    if(childs[j] = fork()){
                        nivel++;
                        break;
                    }
                }
            }
            break;
        }
        
    }
    if(j == 2){    
        for( k = 0; k < 2; k++)
        {
            if (!(childs[k] = fork())) {
                nivel++;
                break;
            }    
        }
        
    }

    if(getpid() != padre){//hijos
        if( i == 2){
          for(int a = 0; a < 11; a++)
            {
                if(a != 0)close(fd[a][0]);
                if(a != 1)close(fd[a][1]);
            }
            
            n = read(fd[0][0],buffer,sizeof(buffer));
            buffer[n] = '\0';
            strcat(buffer,"D");
            write(fd[1][1],buffer,strlen(buffer));
            close(fd[0][0]);
            close(fd[1][1]);
        }
        if(i == 0){
            for(int a = 0; a < 11; a++)
                {
                    if(a != 9 )close(fd[a][0]);
                    if(a != 10 )close(fd[a][1]);
                }
                n = read(fd[9][0],buffer,sizeof(buffer));
                buffer[n] = '\0';
                strcat(buffer,"B");
                write(fd[10][1],buffer,strlen(buffer));
                close(fd[9][0]);
                close(fd[10][1]);
        }
        if (i == 1 && j == 0) {
            
            //printf("soy %d ",getpid());
            for(int a = 0; a < 11; a++)
                {
                    if(a != 1 && a != 8)close(fd[a][0]);
                    if(a != 2 && a != 9)close(fd[a][1]);
                }
                n = read(fd[1][0],buffer,sizeof(buffer));
                buffer[n] = '\0';
                strcat(buffer,"C");
                write(fd[2][1],buffer,strlen(buffer));
                
                n = read(fd[8][0],buffer,sizeof(buffer));
                buffer[n] = '\0';
                strcat(buffer,"C");
                write(fd[9][1],buffer,strlen(buffer));                
                close(fd[1][0]);
                close(fd[2][1]);        
                close(fd[8][0]);
                close(fd[9][1]);        
        }
        if(j == 1){
            //printf("soy %d ",getpid());
            
            for(int a = 0; a < 11; a++)
                {
                    if(a != 2 && a != 7)close(fd[a][0]);
                    if(a != 3 && a != 8)close(fd[a][1]);
                }
                n = read(fd[2][0],buffer,sizeof(buffer));
                buffer[n] = '\0';
                strcat(buffer,"E");
                write(fd[3][1],buffer,strlen(buffer));
                
                n = read(fd[7][0],buffer,sizeof(buffer));
                buffer[n] = '\0';
                strcat(buffer,"E");
                write(fd[8][1],buffer,strlen(buffer));                
                
                close(fd[2][0]);
                close(fd[3][1]);        
                close(fd[7][0]);
                close(fd[8][1]);  
        }
        if(j == 2 && k == 2){
            
            //printf("soy %d ",getpid());
            for(int a = 0; a < 11; a++)
                {
                    if(a != 3 && a != 6)close(fd[a][0]);
                    if(a != 4 && a != 7)close(fd[a][1]);
                }
                n = read(fd[3][0],buffer,sizeof(buffer));
                buffer[n] = '\0';
                strcat(buffer,"F");
                write(fd[4][1],buffer,strlen(buffer));
                //for( int a = 0; a < 2; a++) sleep(1);
                n = read(fd[6][0],buffer,sizeof(buffer));
                buffer[n] = '\0';
                strcat(buffer,"F");
                write(fd[7][1],buffer,strlen(buffer));                
                
                close(fd[3][0]);
                close(fd[4][1]);        
                close(fd[6][0]);
                close(fd[7][1]);  
        }
        if( k == 1){
            //printf("soy %d ",getpid());
            for(int a = 0; a < 11; a++)
                {
                    if(a != 4 )close(fd[a][0]);
                    if(a != 5 )close(fd[a][1]);
                }
                n = read(fd[4][0],buffer,sizeof(buffer));
                buffer[n] = '\0';
                strcat(buffer,"H");
                write(fd[5][1],buffer,strlen(buffer));
                
                close(fd[4][0]);
                close(fd[5][1]);
        }
        if( k == 0){
            //printf("soy %d ",getpid());
            for(int a = 0; a < 11; a++)
                {
                    if(a != 5 )close(fd[a][0]);
                    if(a != 6 )close(fd[a][1]);
                }
                n = read(fd[5][0],buffer,sizeof(buffer));
                buffer[n] = '\0';
                strcat(buffer,"G");
                write(fd[6][1],buffer,strlen(buffer));
                close(fd[5][0]);
                close(fd[6][1]);
        }
    }else{//padre
        for(int a = 0; a < 11; a++)
        {
            if(a != 10)close(fd[a][0]);
            if(a != 0)close(fd[a][1]);
        }
        strcpy(buffer,"A");
        write(fd[0][1],buffer,strlen(buffer));
        for( int a = 0; a < 3; a++) wait(NULL);
        n = read(fd[10][0],buffer,sizeof(buffer));
        buffer[n] = '\0';
        strcat(buffer,"A");
        //memcpy(buff, buffer, 1024*sizeof(char));
        printf("n=%d\n",nimp);
        for(int x = 0; x < nimp; x++)
        {
            printf("%s\n",buffer);
        }               
        close(fd[0][1]);
        close(fd[10][0]);
    }
    
    //printf("i:%d j:%d k:%d %d nivel:%d\n",i,j,k,getpid(),nivel);
    if(padre==getpid()){   
        char b[500];
        sprintf(b,"pstree -lp %d",getpid());
        system(b);
        
    }else{
        sleep(2);
    }
    return 0;
}
