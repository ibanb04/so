#include<unistd.h>
#include<wait.h>
#include<sys/shm.h>
#include<sys/stat.h>
#include<stdio.h>
#include <sys/types.h>
#include <string.h>
#include <stdlib.h>

int main()
{
    pid_t padre = getpid();
    
    int fd[2][2], i;

    for( i = 0; i < 2; i++)
    {
        if(pipe(fd[i])) exit(-1);
    }
    for(i = 0; i < 2; i++) if(!(fork())) break;
    
    if (i == 1) {
        for(int j = 0; j < 2; j++){
            close(fd[j][0]);
            if(j!=1)close(fd[j][1]);
        }
        FILE *f = fopen("vocales.txt","r");
        fseek(f,0,SEEK_END);
        int tam = ftell(f);
        char info[tam];
        fseek(f,0,0);
        for(int a = 0; a < tam; a++)
        {
            info[a] = getc(f);
        }
    
        write(fd[i][1],&tam,sizeof(int));
        write(fd[i][1],info,sizeof(char)*tam);
        printf("H%d he terminado de enviar todo el archivo \n",i+1);
        fclose(f);
        close(fd[i][1]);
        /*for(int i = 0; i < tam; i++)
        {
            printf("%c",info[i]);
        }*/
    }else{
        if(i == 0){
            for(int j = 0; j < 2; j++){
                if(j!=1)close(fd[j][0]);
                if(j!=0)close(fd[j][1]);
            }
            int tam, n_vocals = 0;
            read(fd[1][0],&tam,sizeof(int));
            char info[tam];
            read(fd[1][0],info,sizeof(char)*tam);
            for(int k = 0; k < tam; k++){
                if('a' == info[k] || 'e' == info[k] || 'i' == info[k] || 'o' == info[k] || 'u' == info[k]) n_vocals++;
                if('A' == info[k] || 'E' == info[k] || 'I' == info[k] || 'O' == info[k] || 'U' == info[k]) n_vocals++;
            }
            printf("H%d el numero de vocales es :%d\n",i+1,n_vocals);
            write(fd[i][1],&tam,sizeof(int));
            write(fd[i][1],info,sizeof(char)*tam);
            close(fd[1][0]);
            close(fd[i][1]);
        }
        if (padre == getpid() && i == 2 ) {
            for(int j = 0; j < 2; j++){
                if(j!=0)close(fd[j][0]);
                close(fd[j][1]);
            }
                int tam, n_espacios = 0;
                read(fd[0][0],&tam,sizeof(int));
                char info[tam];
                read(fd[0][0],info,sizeof(char)*tam);
                
                for(int k  = 0; k < tam;k++)
                {
                    if(info[k] == '\n') n_espacios++;
                }
                printf("padre-> el numero de espacios es: %d\n",n_espacios);
                close(fd[0][0]);   
        }
    }
    
    if(padre==getpid()){
        sleep(1);
        char b[500];
        sprintf(b,"pstree -lp %d",getpid());
        system(b);
        for(i = 0; i < 2; i++) wait(NULL);
    }else{
        sleep(1);
        }
    return 0;
}
