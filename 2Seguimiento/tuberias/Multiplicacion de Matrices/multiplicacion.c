#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>

int main()
{
    
    int n;
    printf("digite n:\n");
    scanf("%d",&n);
    int fd[4][2],cont = 0,i;
    pid_t padre = getpid();
    
    
    for( i = 0; i < 4; i++)
    {    if(pipe(fd[i])){
            printf("no se pudieron crear las tuberias");
            exit(-1);
        }
    }
    
    for( i = 0; i < 3; i++) if(!(fork()))break;
    
    if (padre == getpid()) {
        for(int j = 0; j < 4; j++)
        {
            if(j!=3)close(fd[j][0]);
            if(j!=0) close(fd[j][1]);
        }
        int a[n][n];
        int b[n][n];
        int c[n][n];
        for(int r = 0; r < n; r++){
            for(int c = 0; c < n; c++){
                a[r][c] = cont;
                b[r][c] = cont;
                cont++;
            }
        }
        printf("las matrices A y B son:\n");
        printf("A\n");
        for(int r = 0; r < n; r++){
            for(int c = 0; c < n; c++){
                printf("[%d]",a[r][c]);
            }
            printf("\n");
        }
        printf("B\n");
        for(int r = 0; r < n; r++){
            for(int c = 0; c < n; c++){
                printf("[%d]",b[r][c]);
            }
            printf("\n");
        }
        printf("\n"); 
        write(fd[0][1],a,sizeof(int)*(n*n));
        write(fd[0][1],b,sizeof(int)*(n*n));
        write(fd[0][1],c,sizeof(int)*(n*n));

        //int result[n][n];
        read(fd[3][0],c,sizeof(int)*(n*n));
        printf("soy el padre y la matriz resultante es\n"); 
        for(int r = 0; r < n; r++)
        {
            for(int a = 0; a < n; a++){
            printf("[%d]",c[r][a]);
            }
        printf("\n");
        }
        printf("\n");                        
        close(fd[0][1]);
        close(fd[3][0]);
    }else{
        if(i == 0)
        {

            for(int j = 0; j < 4; j++)
            {
                if(j!=0) close(fd[j][0]);
                if(j!=1) close(fd[j][1]);
            }
            int a[n][n];
            int b[n][n];
            int result[n][n];
            read(fd[i][0],a,sizeof(int)*(n*n));
            read(fd[i][0],b,sizeof(int)*(n*n));
            read(fd[i][0],result,sizeof(int)*(n*n));
            
            for(int r=1;r<n;r++){
                for(int j=0;j<r;j++){
                    result[r][j]=0;
                    for(int k=0;k<n;k++){
                        result[r][j]=(result[r][j]+(a[r][k]*b[k][j]));
                    }
                }
            }
            printf("soy el hijo%d\n",i+1); 
            for(int r = 0; r < n; r++)
            {
                for(int c = 0; c < n; c++){
                printf("[%d]",result[r][c]);
                }
            printf("\n");
            }
            printf("\n");
            write(fd[i+1][1],a,sizeof(int)*(n*n));
            write(fd[i+1][1],b,sizeof(int)*(n*n));
            write(fd[i+1][1],result,sizeof(int)*(n*n));
            close(fd[i][0]);
            close(fd[i+1][0]);
        }
        if (i == 1) {

            for(int j = 0; j < 4; j++)
            {
                if(j!=1) close(fd[j][0]);
                if(j!=2) close(fd[j][1]);
            }
            int a[n][n];
            int b[n][n];
            int result[n][n];
            read(fd[i][0],a,sizeof(int)*(n*n));
            read(fd[i][0],b,sizeof(int)*(n*n));
            read(fd[i][0],result,sizeof(int)*(n*n));

            for(int r=0;r<n;r++){
                for(int c=r+1;c<n;c++){
                    result[r][c]=0;
                    for(int k=0;k<n;k++){
                        result[r][c]=(result[r][c]+(a[r][k]*b[k][c]));
                        }
                    }
            }
            printf("soy el hijo%d\n",i+1); 
            for(int r = 0; r < n; r++)
            {
                for(int c = 0; c < n; c++)
                {
                printf("[%d]",result[r][c]);
                }
            printf("\n");
            }

            printf("\n");        
            write(fd[i+1][1],a,sizeof(int)*(n*n));
            write(fd[i+1][1],b,sizeof(int)*(n*n));
            write(fd[i+1][1],result,sizeof(int)*(n*n));
            
            close(fd[i][0]);
            close(fd[i+1][0]);
        }if(i == 2) 
        {
            for(int j = 0; j < 4; j++)
            {
                if(j!=2) close(fd[j][0]);
                if(j!=3) close(fd[j][1]);
            }
            int a[n][n];
            int b[n][n];
            int result[n][n];
            read(fd[i][0],a,sizeof(int)*(n*n));
            read(fd[i][0],b,sizeof(int)*(n*n));
            read(fd[i][0],result,sizeof(int)*(n*n));
            for(int r=0;r<n;r++)
            {
                for(int c=0;c<n;c++)
                {                  
                    if(r==c)
                    {
                        result[r][c]=0;
                        for(int k=0;k<n;k++)
                        {
                            result[r][c]=(result[r][c]+(a[r][k]*b[k][c]));
                        }
                    }
                }
            }
            printf("soy el hijo%d\n",i+1); 
            for(int r = 0; r < n; r++)
            {
                for(int c = 0; c < n; c++)
                {
                printf("[%d]",result[r][c]);
                }
             printf("\n");
            }            

            write(fd[i+1][1],result,sizeof(int)*(n*n));
            close(fd[i][0]);
            close(fd[i+1][0]);
        }
    
    }

    if(padre==getpid()){
        char b[500];
        sprintf(b,"pstree -lp %d",getpid());
        system(b);
        for( i = 0; i < 3; i++) wait(NULL);
    }else{
        sleep(2);  
    }
    return 0;
}
