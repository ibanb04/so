#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


int main(int argc, char const *argv[])
{
    int fd[5][2], i = 0, j=-1, nivel=0;
    pid_t childs[3], padre= getpid();

    for( i = 0; i < 5; i++)
    {
        if(pipe(fd[i])){
            printf("Las tuberias no fueron creadas");
            return -1;
        }
    }

    for( i = 0; i < 3; i++){
        if(!(childs[i] = fork())){
            if (i == 2) {
                for( j = 0; j < 2; j++)
                {
                    
                    if (childs[j] = fork()) {
                    
                        break;
                    }
                }
            } 
            break;
        } 
    }
    
    if (getpid() == padre) {
        
        for( i = 0; i < 5; i++)
        {   
            
            if (i !=4) {
                close(fd[i][0]);
                close(fd[i][1]);
            }else{
                close(fd[i][1]);
            }
            
        }
      /*  
        close(fd[0][0]);
        close(fd[0][1]);
        close(fd[1][0]);
        close(fd[1][1]);
        close(fd[2][0]);
        close(fd[2][1]);
        close(fd[3][0]);
        close(fd[3][1]);
        close(fd[4][1]);
       */
        int n, cant_Mayus, cant_Minus, cant_Space, cant_Puntos, cant_Comas;
        read(fd[4][0], &n, sizeof(int));
        char informacion[n];
        read(fd[4][0], informacion, n);
        read(fd[4][0], &cant_Mayus, sizeof(int));
        read(fd[4][0], &cant_Minus, sizeof(int));
        read(fd[4][0], &cant_Space, sizeof(int));
        read(fd[4][0], &cant_Puntos, sizeof(int));
        read(fd[4][0], &cant_Comas, sizeof(int));
        close(fd[4][0]);

        printf("Este es el archivo %s\n", informacion);
        printf("Cantidad de Mayusculas: %d \n", cant_Mayus);
        printf("Cantidad de Minusculas: %d \n", cant_Minus);
        printf("Cantidad de Espacios: %d \n", cant_Space);
        printf("Cantidad de Puntos: %d \n", cant_Puntos);
        printf("Cantidad de Comas: %d \n", cant_Comas);

        
        
    }else{
        if (getppid() == padre) {
            
            if (i == 0) {
                /*
                for( i = 0; i < 4; i++)
                {
                    close(fd[i][0]);
                    if(i==0) continue;
                    close(fd[i][1]);
                }
                */
                close(fd[0][0]);
                close(fd[1][0]);
                close(fd[1][1]);
                close(fd[2][0]);
                close(fd[2][1]);
                close(fd[3][0]);
                close(fd[3][1]);
                close(fd[4][0]);
                close(fd[4][1]);
                FILE * f = fopen("datos.txt", "r");
                if(f){
                    fseek(f,0,2);
                    int n = ftell(f);
                    char linea[n], infoArchivo[n];
                    strcpy(infoArchivo, "");
                    fseek(f,0,0);
                    while(!feof(f)){
                        fgets(linea,n,f);
                        strcat(infoArchivo,linea);
                    }
                    int cant_Mayus=0;
                    for( i = 0; i < n; i++)
                    {
                        
                        if (infoArchivo[i] >= 'A' && infoArchivo[i] <= 'Z') {
                            cant_Mayus++;
                        }
                        
                    }
                    write(fd[0][1], &n, sizeof(int));
                    write(fd[0][1], infoArchivo, n);
                    write(fd[0][1], &cant_Mayus, sizeof(int));
                    close(fd[0][1]);
                    
                    
                }
            
            }
            
            if (i == 1) {
                close(fd[0][1]);
                close(fd[1][0]);
                close(fd[2][0]);
                close(fd[2][1]);
                close(fd[3][0]);
                close(fd[3][1]);
                close(fd[4][0]);
                close(fd[4][1]);

                int n, cant_Mayus, cant_Minus=0;
                read(fd[0][0], &n, sizeof(int));
                char informacion[n];
                read(fd[0][0], informacion, n);
                read(fd[0][0], &cant_Mayus, sizeof(int));
                
                for( i = 0; i < n; i++)
                {
                    if (informacion[i] >= 'a' && informacion[i] <= 'z') {
                        cant_Minus++;
                    }
                    
                }
                 write(fd[1][1], &n, sizeof(int));
                 write(fd[1][1], informacion, n);
                 write(fd[1][1], &cant_Mayus, sizeof(int));
                 write(fd[1][1], &cant_Minus, sizeof(int));
                 close(fd[0][0]);
                 close(fd[1][1]);
                
            }
            
            if (i == 2) {
                close(fd[0][0]);
                close(fd[0][1]);
                close(fd[1][1]);
                close(fd[2][0]);
                close(fd[3][0]);
                close(fd[3][1]);
                close(fd[4][0]);
                close(fd[4][1]);

                int n, cant_Mayus, cant_Minus, cant_Space=0;
                read(fd[1][0], &n, sizeof(int));
                char informacion[n];
                read(fd[1][0], informacion, n);
                read(fd[1][0], &cant_Mayus, sizeof(int));
                read(fd[1][0], &cant_Minus, sizeof(int));
                
                for( i = 0; i < n; i++)
                {
                    
                    if (informacion[i] == ' ') {
                        cant_Space++;
                    }
                    
                }
                write(fd[2][1], &n, sizeof(int));
                write(fd[2][1], informacion, n);
                write(fd[2][1], &cant_Mayus, sizeof(int));
                write(fd[2][1], &cant_Minus, sizeof(int));
                write(fd[2][1], &cant_Space, sizeof(int));
                close(fd[1][0]);
                close(fd[2][1]);
            }
            
            
        
    }
    
    if (j == 1) {
        close(fd[0][0]);
        close(fd[0][1]);
        close(fd[1][0]);
        close(fd[1][1]);
        close(fd[2][1]);
        close(fd[3][0]);
        close(fd[4][0]);
        close(fd[4][1]);
        
        int n, cant_Mayus, cant_Minus, cant_Space, cant_Puntos=0;
        read(fd[2][0], &n, sizeof(int));
        char informacion[n];
        read(fd[2][0], informacion, n);
        read(fd[2][0], &cant_Mayus, sizeof(int));
        read(fd[2][0], &cant_Minus, sizeof(int));
        read(fd[2][0], &cant_Space, sizeof(int));
        
        for( i = 0; i < n; i++)
        {
            
            if (informacion[i] == '.') {
                cant_Puntos++;
            }
            
        }
        write(fd[3][1], &n, sizeof(int));
        write(fd[3][1], informacion, n);
        write(fd[3][1], &cant_Mayus, sizeof(int));
        write(fd[3][1], &cant_Minus, sizeof(int));
        write(fd[3][1], &cant_Space, sizeof(int));
        write(fd[3][1], &cant_Puntos, sizeof(int));
        close(fd[2][0]);
        close(fd[3][1]);
        
        
        
    }
    
    if (j == 2) {
        close(fd[0][0]);
        close(fd[0][1]);
        close(fd[1][0]);
        close(fd[1][1]);
        close(fd[2][0]);
        close(fd[2][1]);
        close(fd[3][1]);
        close(fd[4][0]);
        
        int n, cant_Mayus, cant_Minus, cant_Space, cant_Puntos, cant_Comas= 0;
        read(fd[3][0], &n, sizeof(int));
        char informacion[n];
        read(fd[3][0], informacion, n);
        read(fd[3][0], &cant_Mayus, sizeof(int));
        read(fd[3][0], &cant_Minus, sizeof(int));
        read(fd[3][0], &cant_Space, sizeof(int));
        read(fd[3][0], &cant_Puntos, sizeof(int));
        
        for( i = 0; i < n; i++)
        {
            
            if (informacion[i] == ',') {
                cant_Comas++;
            }
            
        }
        write(fd[4][1], &n, sizeof(int));
        write(fd[4][1], informacion, n);
        write(fd[4][1], &cant_Mayus, sizeof(int));
        write(fd[4][1], &cant_Minus, sizeof(int));
        write(fd[4][1], &cant_Space, sizeof(int));
        write(fd[4][1], &cant_Puntos, sizeof(int));
        write(fd[4][1], &cant_Comas, sizeof(int));
        close(fd[3][0]);
        close(fd[4][1]);

    }
    
    
    }
    

//IMPRESION
if(padre==getpid()){
    char b[500];
    sprintf(b,"pstree -lp %d",getpid());
    system(b);
}else{
    sleep(1);
}
  // IMPRESION::
    return 0;
}