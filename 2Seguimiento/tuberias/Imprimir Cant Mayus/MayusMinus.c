#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <wait.h>

int main(int argc, char const *argv[])
{
    
    int i, j = 0, k = 0, fd[6][2];
    pid_t childs[3], padre = getpid();
    for(i = 0; i < 6; i++) if(pipe(fd[i])) exit(-1);
    //creando la jerarquia
    for(i = 0; i < 3; i++){
        if(!(childs[i] = fork())) {
            if(i == 2){
                for( j = 0; j < 2; j++)
                {
                    if(childs[j] = fork()) break;
                }            
            }
            break;
        }   
    }
    
    if(getpid() == padre){

        close(fd[0][0]);
        close(fd[1][0]);
        close(fd[2][0]);
        close(fd[2][1]);
        close(fd[3][0]);
        close(fd[3][1]);
        close(fd[4][0]);
        close(fd[4][1]);
        close(fd[5][1]);        

        FILE *archivo = fopen("prueba.txt", "r");
        
        if (!archivo) {
            printf("error al cargar el archivo \n");
            exit(-1);
        }
        fseek(archivo,0,2);
        int tam = ftell(archivo);
        fseek(archivo,0,0);
        char info[tam];
        
        for( k = 0; k < tam; k++)
        {
            info[k] = getc(archivo);
        }
        int n_mayus , n_minus , n_espacios ,n_puntos , n_comas ;
        read(fd[5][0],&n_mayus,sizeof(int));
        read(fd[5][0],&n_minus,sizeof(int));                 
        read(fd[5][0],&n_espacios,sizeof(int));                
        read(fd[5][0],&n_puntos,sizeof(int));
        printf("Este es el archivo %s\n", info);
        printf("Cantidad de Mayusculas: %d \n", n_mayus);
        printf("Cantidad de Minusculas: %d \n", n_minus);
        printf("Cantidad de Espacios: %d \n", n_espacios);
        printf("Cantidad de Puntos: %d \n", n_puntos);
        printf("Cantidad de Comas: %d \n", n_comas);
        write(fd[0][1],&tam,sizeof(int));
        write(fd[0][1],info,tam);

        fclose(archivo);
        close(fd[0][1]);
        close(fd[5][0]);

    }else{
        
        if (getppid() == padre) {
            if(i == 0){
                 
                close(fd[0][1]);
                close(fd[1][0]);
                close(fd[2][0]);
                close(fd[2][1]);
                close(fd[3][0]);
                close(fd[3][1]);
                close(fd[4][0]);
                close(fd[4][1]);
                close(fd[5][0]);
                close(fd[5][1]);
                
                int tam;
                read(fd[0][0],&tam,sizeof(int));
                char info[tam];
                read(fd[0][0],info,tam);
                int n_mayus = 0;
                for( k = 0; k < tam; k++)
                {
                    if(info[k] >= 'A' && info[k] <= 'Z') n_mayus++;
                }
                printf("proceso: %d info: %s \n",getpid(),info);
                write(fd[1][1],&tam,sizeof(int));
                write(fd[1][1],info,tam);
                write(fd[1][1],&n_mayus,sizeof(int));
                close(fd[0][0]);
                close(fd[1][1]);
            }
                
            if (i == 1) {
                
                close(fd[0][0]);
                close(fd[0][1]);
                
                close(fd[1][1]);
                close(fd[2][0]);
                
                close(fd[3][0]);
                close(fd[3][1]);
                close(fd[4][0]);
                close(fd[4][1]);
                close(fd[5][0]);
                close(fd[5][1]);
                int tam, n_minus = 0, n_mayus;
                read(fd[1][0],&tam,sizeof(int));
                char info[tam];
                read(fd[1][0],info,tam);
                for(k = 0; k < tam; k++){
                    if(info[k] >= 'a' && info[k] <= 'z') n_minus++;
                }
                read(fd[1][0],&n_mayus,sizeof(int));
                write(fd[2][1],&tam,sizeof(int));
                write(fd[2][1],info,tam);
                write(fd[2][1],&n_mayus,sizeof(int));
                write(fd[2][1],&n_minus,sizeof(int));
                close(fd[1][0]);
                close(fd[2][1]);
                }
                
            if (i == 2) {
                
                close(fd[0][0]);
                close(fd[0][1]);
                close(fd[1][0]);
                close(fd[1][1]);
                
                close(fd[2][1]);
                close(fd[3][0]);
                
                close(fd[4][0]);
                close(fd[4][1]);
                close(fd[5][0]);
                close(fd[5][1]);   

                 int tam, n_mayus , n_minus , n_espacios = 0;
                 read(fd[2][0],&tam,sizeof(int));
                 char info[tam];
                 read(fd[2][0],info,tam);
                  
                 for( k = 0; k < tam; k++)
                 {
                 if(info[k] == '\n') n_espacios++; 
                 }
                  
                 read(fd[2][0],&n_mayus,sizeof(int));
                 read(fd[2][0],&n_minus,sizeof(int));
                 write(fd[3][1],&tam,sizeof(int));
                 write(fd[3][1],info,tam);
                 write(fd[3][1],&n_mayus,sizeof(int));
                 write(fd[3][1],&n_minus,sizeof(int));
                 write(fd[3][1],&n_espacios,sizeof(int));
                 
                 close(fd[2][0]);
                 close(fd[3][1]);
                }
        }
        if(j == 1){
            close(fd[0][0]);
            close(fd[0][1]);
            close(fd[1][0]);
            close(fd[1][1]);
            close(fd[2][0]);
            close(fd[2][1]);
            
            close(fd[3][1]);
            close(fd[4][0]);
            
            close(fd[5][0]);
            close(fd[5][1]);

            int tam, n_mayus, n_minus, n_espacios,n_puntos = 0;
                 read(fd[3][0],&tam,sizeof(int));
                 char info[tam];
                 read(fd[3][0],info,tam);
                 for( k = 0; k < tam; k++)
                 {
                 if(info[k] == '.') n_puntos++; 
                 }
                  
                 read(fd[3][0],&n_mayus,sizeof(int));
                 read(fd[3][0],&n_minus,sizeof(int));                 
                 read(fd[3][0],&n_espacios,sizeof(int));                

                 write(fd[4][1],&tam,sizeof(int));
                 write(fd[4][1],info,tam);
                 write(fd[4][1],&n_mayus,sizeof(int));
                 write(fd[4][1],&n_minus,sizeof(int));
                 write(fd[4][1],&n_espacios,sizeof(int)); 
                 write(fd[4][1],&n_puntos,sizeof(int));

                close(fd[3][0]);
                close(fd[4][1]); 

        } 
        if(j == 2){
            close(fd[0][0]);
            close(fd[0][1]);
            close(fd[1][0]);
            close(fd[1][1]);
            close(fd[2][0]);
            close(fd[2][1]);
            close(fd[3][0]);
            close(fd[3][1]);
            
            close(fd[4][1]);
            close(fd[5][0]);
            


            int tam, n_mayus, n_minus, n_espacios,n_puntos, n_comas = 0;
                 read(fd[4][0],&tam,sizeof(int));
                 char info[tam];
                 read(fd[4][0],info,tam);
                 for( k = 0; k < tam; k++)
                 {
                 if(info[k] == ',') n_comas++; 
                 }
                  
                 read(fd[4][0],&n_mayus,sizeof(int));
                 read(fd[4][0],&n_minus,sizeof(int));                 
                 read(fd[4][0],&n_espacios,sizeof(int));                
                 read(fd[4][0],&n_puntos,sizeof(int));                

                 write(fd[5][1],&tam,sizeof(int));
                 write(fd[5][1],info,tam);
                 write(fd[5][1],&n_mayus,sizeof(int));
                 write(fd[5][1],&n_minus,sizeof(int));
                 write(fd[5][1],&n_espacios,sizeof(int)); 
                 write(fd[5][1],&n_puntos,sizeof(int));
                 write(fd[5][1],&n_comas,sizeof(int));

                close(fd[4][0]);
                close(fd[5][1]); 
        }

    }

    //IMPRESION
if(padre==getpid()){
    char b[500];
    sprintf(b,"pstree -lp %d",getpid());
    system(b);
}else{
    sleep(1);
}
    return 0;
}


