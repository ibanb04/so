#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>

int **a, *b, *result, n = 0,m = 0, fActual = 0, nthreads;
void *thread_routine(void *param){
    
    int fila = fActual++;
    if(fila < m){
    for(int i = 0; i < n; i++)
        {
            result[i] += a[fila][i]*b[i];
        }
    }
    if(fila < m){
        thread_routine(NULL);
    }
    pthread_exit(0);
}
int main()
{
    printf("ingrese el numero de filas\n");
    scanf("%d",&m);

    printf("ingrese el numero de columnas\n");
    scanf("%d",&n);
    int temp = 0;
    a = (int**)malloc(m*(sizeof(int *)));
    b = (int*)malloc(n*(sizeof(int)));
    result = (int*)malloc(n*(sizeof(int)));
    for(int i=0; i<n; i++){
        a[i] = (int*) malloc(n*sizeof(int));
    }
    
    for(int i = 0; i < m; i++)
    {
        for(int j = 0; j < n; j++)
        {            
            a[i][j] = temp;
            temp++;
        }
        printf("\n");
    }
    printf("matriz A:\n");
    for(int i = 0; i < m; i++)
    {
        for(int j = 0; j < n; j++)
        {            
            printf("%d\t",a[i][j]);

        }
        printf("\n");
    }
    printf("matriz B:\n");
    int aux = 0;
    for(int i = 0; i < n; i++)
    {
            b[i]= aux,printf("%d\t",b[i]);
            aux++;
    }    
     int nthreads;
    printf("ingrese el numero de hilos\n");
    scanf("%d",&nthreads);
    pthread_t th_id[nthreads];
    pthread_t threads[nthreads];


    for (int k=0; k<nthreads; k++){
        pthread_create(&threads[k], NULL, thread_routine, NULL);
        sleep(2);
    }
    for (int k=0; k<nthreads; k++){
        pthread_join(threads[k], NULL);
    }        
    printf("matriz resultante es:\n");

        for(int j = 0; j < n; j++)
        {
            printf("%d\t",result[j]);
        }

    return 0;
}