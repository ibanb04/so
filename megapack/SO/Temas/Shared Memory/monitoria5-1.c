#include <stdlib.h>
#include <string.h>
#include <sys/shm.h>
#include <stdio.h>
#include <unistd.h>
#include <wait.h>
#include <sys/ipc.h>

int sumar(int *sw, int nHijos){

	int valReturn = 0;
	for (int i = 0; i < nHijos; i++)
	{
		valReturn+=sw[i];
		//printf("%d\n", valReturn );
	}
	return valReturn;
}


int main(int argc, char const *argv[])
{
	int nHijos = 0;
	int k = 0;

	printf("Ingrese el numero de subprocesos: ");
	scanf("%d", &nHijos);

	printf("Ingrese el numero de iteraciones: " );
	scanf("%d", &k);

	printf("\n");

	int padre = getpid();
	pid_t hijos[nHijos];
	int shm_id;
	int shm_size= nHijos*sizeof(int);
	int *sw;

	shm_id = shmget(IPC_PRIVATE, shm_size, IPC_CREAT | 0666);
	sw = shmat(shm_id,NULL,0);

	for (int i = 0; i < nHijos; i++)
	{
		hijos[i] = fork();
		if (hijos[i] == 0)
		{
			for (int j = 0; j < k; j++)
			{
				printf("hijo %d \n", i);
				sw[i] = 1;
				while(sw[i]==1){}
			}
			sw[i] = 1;
			shmdt(sw);
			break;
		}
	}

	if (padre == getpid())
	{
		for (int j = 0; j < k; j++)
		{	
				while(sumar(sw,nHijos)!=nHijos){}
				printf("Padre\n\n");
				for (int i = 0; i < nHijos; i++)
				{
					sw[i] = 0;
				}
				while(sumar(sw,nHijos)!=nHijos){}
		}

		for (int i = 0; i < nHijos; ++i)
		{
			wait(NULL);
		}

		shmdt(sw);
		shmctl(shm_id,IPC_RMID,0);

	}

	return 0;
}