#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

void *funcion_hilo(void *param);
void algo(int);
int sum;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;


int main(int argc, char const *argv[])
{
	int i;
	pthread_t thread_id;

	sum=0;
	printf("previa creacion hilos sum=%d\n",sum);

	pthread_create (&thread_id,NULL,funcion_hilo,NULL);
	for (i = 0; i < 10000; ++i)
	{
		algo(16);
		pthread_mutex_lock(&mutex);
		sum ++;
		pthread_mutex_unlock(&mutex);
			}

	pthread_join(thread_id,NULL);
	printf("hilo principal sum=%d\n",sum);	
	pthread_mutex_destroy(&mutex);	
	return 0;
}

void * funcion_hilo(void *param)
{
int i;
printf("hilo %lu\n",pthread_self());

for (i = 0; i < 10000; ++i)
{
	pthread_mutex_lock(&mutex);
	sum++;
	algo(16);
	pthread_mutex_unlock(&mutex);
}

pthread_exit(0);

}

void algo(int n){
	usleep(n);
}
