#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>


void *contarPuntos(void * param);
int numpuntos, numhilos;
double PI = 0.0, x, y;
int Circle_Count = 0;
double puntoshilo = 0.0;
double *a;

struct Identificador{
	int id;
};

int main() {
    
    printf("Ingrese el número de puntos: ");
    scanf ("%d", &numpuntos);
    
    printf("Ingrese el número de hilos: ");
    scanf ("%d", &numhilos);
    
    struct timespec tic, toc;
    double Elapsed;
    clock_gettime(CLOCK_REALTIME, &tic);

    int i;
	pthread_t pthread_id[numhilos];
    struct Identificador identificador;
    
    a = (double *) calloc (numhilos, sizeof(double));
    for(int i=0; i<numhilos; i++){
        a[i]=0;
    }   
    //printf("Creador el vector contador para cada hilo (inicializados en 0)\n");
    
    puntoshilo = numpuntos/numhilos;
       
    for(i=0; i<numhilos; i++){
		identificador.id = i;
		pthread_create (&pthread_id[i], NULL, contarPuntos, (void*) &identificador);	
	}
    
    for(i=0; i<numhilos; i++){
		pthread_join (pthread_id[i], NULL);	
	}

    for(i=0; i<numhilos; i++){
        Circle_Count += a[i];
    }

    PI= (4.0*Circle_Count)/ (double) numpuntos;   

	printf("Hilo principal PI=%f\n", PI);

    clock_gettime(CLOCK_REALTIME, &toc);
    Elapsed = (toc.tv_sec-tic.tv_sec)+((toc.tv_nsec-tic.tv_nsec)/(double)1E9);
    printf ("Tiempo de ejecución: %.6lf\n", Elapsed);
    return 0;        
}

void * contarPuntos(void *param){
	int i;
	struct Identificador *identificador = (struct Identificador*) param;
    for(i=0; i<puntoshilo; i++){
        x= (double) rand()/(double) RAND_MAX;
        y= (double) rand()/(double) RAND_MAX;
        if((x*x+y*y)<=1.0){
            a[identificador->id]++;        
        }  
   
	}
	pthread_exit(0);
}

