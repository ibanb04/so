#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
  
int turnoglobal;
  
void *fm(void *);
  
int main(){
 
    //int m[2][3]={(0,1,1),(0,2,3)};
    int i,*param; 
    pthread_t hilos[3];
    turnoglobal=0;
    //printf("%d\n",m[0][3]);
  
    for (i = 0; i < 3; ++i)
    {
        param=(int*)malloc(sizeof(int));
        *param= i+1;
        pthread_create(&hilos[i],NULL,fm,(void*)param);
    }
    printf("Padre");
    turnoglobal++;
    for (i = 0; i < 3; ++i)
    {
        pthread_join(hilos[i],NULL);
    }
    return 0;
}
  
void *fm(void *arg){
  
    int turno=*(int*)arg;
    while(turno!=turnoglobal);
    printf("Hilo %d\n",turno);
    turnoglobal++;
}