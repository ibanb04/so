#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void *Pi(void *);
void *imprimir(void *);
int N, nhilos;
double suma=0.0, j=1;
pthread_mutex_t m=PTHREAD_MUTEX_INITIALIZER;

int main(){
 
 int i;	
 printf("Numero de Intervalos\n");
 scanf("%d", &N);
 printf("Numero de Hilos\n");
 scanf("%d",&nhilos);

 pthread_t hilos[nhilos];

 for(i=0;i<nhilos-1;i++){
	pthread_create(&hilos[i], NULL, Pi, NULL);
 }

 for(i=0;i<nhilos-1;i++){
 	pthread_join(hilos[i], NULL);
 }

 pthread_create(&hilos[nhilos-1], NULL, imprimir, NULL);
 pthread_join(hilos[nhilos-1], NULL);
 
 return 0;
}

void *Pi(void *arg){

 double x, y;
 double delta= 1/(double)N;
 for(;;){
         pthread_mutex_lock(&m);
	 if(j<=N){
                x=(double)(j-0.5)*delta;
	//	printf("x %f hilo %u\n", x, (unsigned int)pthread_self());
		y=4.0/(1.0+(x*x));
	//	printf("y %f\n", y);
		suma+=y;
	//	printf("suma %f\n", suma);
		j++;
		pthread_mutex_unlock(&m);
        }else{
		pthread_mutex_unlock(&m);
		break;
	}
        

 }		 
 pthread_exit(NULL);
}

void *imprimir(void *arg){
	double	pi=suma*(1/(double)N);	
        printf("pi: %.20f\n", pi);
        pthread_exit(NULL);
}


