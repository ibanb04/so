#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

int turno=0;
void* turnos(void* );


int main(int argc, char const *argv[])
{
	int i, *param;
	pthread_t hilos[10];
	for(i=0; i<10;i++){
		param=(int *)malloc(sizeof(int));
		*param=i+1;
		pthread_create(&hilos[i],NULL,turnos,(void*) param);
	}
	printf("Padre 0\n");
    turno++;

    for(i=0; i<10;i++){
    	pthread_join(hilos[i],NULL);

    }


	return 0;
}


void *turnos(void *arg){
	int t=*(int*)arg;
	while(t!=turno);
	printf("Hilo %d\n",t);
	turno++;

}