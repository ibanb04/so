#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <stdlib.h>


typedef struct Nodo
{
	int id;
}Nodo;



int salir = 0;
int *estado;
int *tiempo;
int *sws;



void* manejador(void *param){

	int id = ((Nodo*)param)->id;
	//	printf("Id %d\n", id);

	while(sws[id]!=1){}
	 while (salir==0)
        {
           printf("Soy la luz %d\n", id);
           if (estado[id]==1)
           {
             printf("La luz %d se va a encender en %d nanosegundos\n",id, tiempo[id]);
             usleep(tiempo[id]);
             printf("La luz %d se encendio\n",id);
           }else{
             printf("La luz %d se va a apagar en %d nanosegundos\n", id,tiempo[id]);
             usleep(tiempo[id]);
             printf("La luz %d se apago\n",id);
           }
           sws[id]=0;
           usleep(100);
           while(sws[id]!=1){}
        }	

	pthread_exit(0);
}

int main(int argc, char const *argv[])
{

	printf("Ingrese el numero de hilos: ");
	int nhilos=0;
	scanf("%d", &nhilos);

	pthread_t pidhilos[nhilos];
	Nodo *nodo[nhilos];


	estado = (int*)calloc(nhilos, sizeof(int));
	tiempo = (int*)calloc(nhilos, sizeof(int));
	sws = (int*)calloc(nhilos, sizeof(int));

	 int id_luz;
     int tiemp;
     int est;

	for (int i = 0; i < nhilos; ++i)
	{
		nodo[i] = (Nodo*)malloc(sizeof(Nodo));
		nodo[i]->id = i;
		estado[i] = 0;
		tiempo[i]=0;
		sws[i]=0;
		pthread_create(&pidhilos[i],NULL, manejador, (void*)nodo[i]);
	}

	  do
	  {
		  	printf("Ingrese el id de la luz: ");
	      scanf("%d", &id_luz);
	      printf("Ingrese el tiempo: ");
	      scanf("%d", &tiemp);
	      printf("Ingrese el estado (0) apagar (1) encender: ");
	      scanf("%d", &est);

	      
	      tiempo[id_luz] = tiemp;
	      estado[id_luz] = est;
	      sws[id_luz] = 1;
	      usleep(100);
	      
	      printf("SALIR:...  ");
	      scanf("%d", &salir);
	      printf("\n\n");

	  } while (salir!=1);
	  

	for (int i = 0; i < nhilos; ++i)
	{
		sws[i] = 1;
		pthread_join(pidhilos[i],NULL);
	}
	return 0;
}
