#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#define nhilos 4


typedef struct Nodo
{
	int identificador;
}Nodo;

////////////////////////////////

void *manjeador(void*);
void fill_matrix_rand(double**,int );
void fill_matrix_empty(double**,int );
void print_matrix(double**,int);
void multiplicar_matrix_superior(int);
void multiplicar_matrix_diagonal(int);
void multiplicar_matrix_inferior(int);
////////////////////////////////

double **M1, **M2, **C;
int n=0;


int main(int argc, char const *argv[])
{
	printf("Ingrese el numero nxn: ");
	scanf("%d", &n);

	printf("\n");

	M1 = (double **) calloc(n, sizeof(double*));
	M2 = (double **) calloc(n, sizeof(double*));
	C = (double **) calloc(n, sizeof(double*));
	for(int i=0;i<n;i++){
		M1[i] = (double *) calloc(n, sizeof(double));
		M2[i] = (double *) calloc(n, sizeof(double));
		C[i] = (double *) calloc(n, sizeof(double));
	}

	fill_matrix_rand(M1,n);
	fill_matrix_rand(M2,n);
	fill_matrix_empty(C,n);

	printf("Matriz A\n");
	print_matrix(M1, n);

	printf("\nMatriz B\n");
	print_matrix(M2, n);

	pthread_t pidhilos[nhilos];
	Nodo *nodo[nhilos];

	for (int i = 0; i < nhilos; ++i)
	{

		nodo[i] = (Nodo *) malloc(sizeof(Nodo));
		nodo[i]->identificador = i;
		pthread_create(&pidhilos[i],NULL,manjeador, (void*) nodo[i]);
    
	}

	for (int i = 0; i < nhilos; ++i)
	{
	
		pthread_join(pidhilos[i], NULL);
    
	}


	return 0;
}


void *manjeador(void *param){

	int id = ((Nodo*) param)-> identificador;
	//printf("Id %d\n", id);
	switch(id){
		case 0:
			multiplicar_matrix_superior(n);
			break;
		case 1:
			multiplicar_matrix_diagonal(n);
			break;
		case 2:
			multiplicar_matrix_inferior(n);
			break;
		case 3:
		printf("\n");
			print_matrix(C,n);
			break;
	}


	pthread_exit(0);
}

void fill_matrix_empty(double **mat,int n){
    int i, k;
    for(i=0; i<n; i++){
        for(k=0; k<n; k++){
            mat[i][k]= 0;
        }
    }
}

void fill_matrix_rand(double **mat,int n){
    int i, k;
    for(i=0; i<n; i++){
        for(k=0; k<n; k++){
            mat[i][k]= rand()%6;
        }
    }
}

void print_matrix(double **mat,int n){
    int i, k;
    for(i=0; i<n; i++){
        for(k=0; k<n; k++){
            printf("[%.1f]", mat[i][k]);
        }
        printf("\n");
    }
    printf("\n");
}

void multiplicar_matrix_superior(int col_fil){
	int i=0, j=0, k=0;
	for ( i = 0; i < col_fil; i++)
	{
		for ( j = col_fil-1; j > i; j--)
		{
			for (k = 0; k < col_fil; k++)
			{
				C[i][j]+=M1[i][k]*M2[k][j];
			}
		}
	}
}

void multiplicar_matrix_diagonal(int col_fil){
	int i=0, k=0;
	for ( i = 0; i < col_fil; i++)
	{
			for (k = 0; k < col_fil; k++)
			{
				C[i][i]+=M1[i][k]*M2[k][i];
			}
	
	}
}

void multiplicar_matrix_inferior(int col_fil){
	int i=0, j=0, k=0;
	for ( i = 1; i < col_fil; i++)
	{
		for ( j = 0; j < i; j++)
		{
			for (k = 0; k < col_fil; k++)
			{
				C[i][j]+=M1[i][k]*M2[k][j];
			}
		}
	}
}