#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define Max_size 10000

typedef struct Nodo
{
	int id;
}Nodo;

void *manejador(void*);

int *lim_inf, *lim_sup;

double global=0.0;

double *a, *b, *resultado;

int main(int argc, char const *argv[])
{
	int nhilos=0;
	int NIH=0;

	printf("Ingrese el numero de hilos: ");
	scanf("%d", &nhilos);
	
	NIH = Max_size/nhilos;

	a = (double*)calloc(Max_size, sizeof(double));
	b = (double*)calloc(Max_size, sizeof(double));
	lim_inf = (int*)calloc(nhilos,sizeof(int));
	lim_sup = (int*)calloc(nhilos,sizeof(int));
	resultado = (double*)calloc(nhilos, sizeof(double));


	for (int i = 0; i < Max_size; ++i)
	{
		a[i] = (i*10)/(double)(i+1);
		b[i] = ((i+2)*10)/(double)(i+1);
	}


	for (int i = 0; i < nhilos; ++i)
	{
		lim_inf[i] = i*NIH;
		lim_sup[i] = lim_inf[i] + NIH;
	}

	pthread_t pidhilos[nhilos];
	Nodo *nodo[nhilos];

	 // Inicio del main
    struct timespec tic, toc;
    double Elapsed;
    clock_gettime(CLOCK_REALTIME, &tic);

    
	for (int i = 0; i < nhilos; ++i)
	{
		nodo[i] = (Nodo*)malloc(sizeof(Nodo));
		nodo[i]->id = i;
		pthread_create(&pidhilos[i],NULL,manejador,(void*) nodo[i]);
	}

	for (int i = 0; i < nhilos; ++i)
	{
		pthread_join(pidhilos[i], NULL);
	}

	//Finalizar el main    
    clock_gettime(CLOCK_REALTIME, &toc);
    Elapsed = (toc.tv_sec-tic.tv_sec)+((toc.tv_nsec-tic.tv_nsec)/(double)1E9);
    printf ("Tiempo de ejecucion: %.6lf\n", Elapsed);


	double suma=0.0, ERM = 0.0;

	for (int i = 0; i < nhilos; ++i)
	{
		suma+=resultado[i];
	}

	ERM = suma/Max_size;

	printf("ERM = %.10f \n", ERM);


	return 0;
}

void *manejador(void *param){

	int id_hilo = ((Nodo*)param)->id;

	for (int i = lim_inf[id_hilo] ; i < lim_sup[id_hilo]; ++i)
	{
		resultado[id_hilo]+=((a[i]-b[i])*(a[i]-b[i]));
		//global+=((a[i]-b[i])*(a[i]-b[i]));
	}

	pthread_exit(0);
}
