#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>


typedef struct Nodo
{
	int id;
	int lim_i;
	int lim_s;
}Nodo;

int n=0;

double **I, **R;

void llenar(double** m){
	for (int i = 0; i < n; ++i)
	{
		for (int j = 0; j < n; ++j)
		{
			m[i][j] = rand()%5;
		}
	}
}

void llenar_cero(double** m){
	for (int i = 0; i < n; ++i)
	{
		for (int j = 0; j < n; ++j)
		{
			m[i][j] = 0;
		}
	}
}

void imprimir(double** m){
	for (int i = 0; i < n; ++i)
	{
		for (int j = 0; j < n; ++j)
		{
			printf("%.2f ",m[i][j]);
		}
		printf("\n");
	}
}

double calcular_filtro(int fila, int col){
	
	double result=I[fila][col];
	int inicio_fil = 0, fin_fil =0 ;
	int inicio_col = 0, fin_col =0 ;
	if ((fila!=0 && fila!=(n-1)) && (col!=0 && col!=(n-1)))
	{
		inicio_fil= fila-1;
		fin_fil = fila+1;
		inicio_col = col-1;
		fin_col = col+1;
		result = 0.0;

		//printf("entrooo\n");
		for (int i = inicio_fil; i < fin_fil; ++i)
		{
			for (int j= inicio_col; j < fin_col; ++j)
			{
				result += I[i][j];
			}
		}
		result=result/9;
		//printf("%f\n", result);
	}


	return result;
}

void* manejador(void *param){

	int idhilo = ((Nodo*)param)->id;
	int inicio = ((Nodo*)param)->lim_i;
	int end = ((Nodo*)param)->lim_s;

	//printf("Inicio: %d\n", inicio);
	//printf("Fin: %d\n", end);

	for (int i = inicio; i < end; ++i)
	{
		for (int j = 0; j < n; ++j)
		{
			R[i][j] = calcular_filtro(i,j);
		}
	}

	pthread_exit(0);
}



int main(int argc, char const *argv[])
{
	
	int nhilos = 0;
	printf("Ingrese el tamaño de la matrix: ");
	scanf("%d", &n);
	printf("Ingrese el numero de hilos: ");
	scanf("%d", &nhilos);
	
	I = (double**)calloc(n,sizeof(double*));
	R = (double**)calloc(n,sizeof(double*));
	for (int i = 0; i < n; ++i)
	{
		I[i] = (double*)calloc(n,sizeof(double));
		R[i] = (double*)calloc(n,sizeof(double));
	}
	printf("\n");
	llenar(I);
	llenar_cero(R);
	imprimir(I);

	

	int nih = n/nhilos;

	pthread_t pidhilos[nhilos];

	Nodo *nodo[nhilos];

	for (int i = 0; i < nhilos; ++i)
	{
		nodo[i] = (Nodo*)malloc(sizeof(Nodo));
		nodo[i]->id = i;
		nodo[i]->lim_i = i*nih;
		nodo[i]->lim_s = (i*nih) + nih;
		//printf("Final : %d\n", nodo[i]->lim_s);
		pthread_create(&pidhilos[i], NULL, manejador, (void*)nodo[i]);
	}

	for (int i = 0; i < nhilos; ++i)
	{
		pthread_join(pidhilos[i], NULL);
	}

	printf("\n");
	imprimir(R);
	return 0;
}