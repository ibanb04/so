#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

void *funcion_hilo(void *param);
void algo(int);
int sum;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

int main()
{
	int i;
	pthread_t thread_id;

	sum=0;
	printf("previa creacion de hilos suma=%d\n",sum );
    
    pthread_create(&thread_id,NULL,funcion_hilo,NULL);
    for (int i = 0; i < 10000; i++)
    {
    	algo(16);
    	pthread_mutex_lock(&mutex);
    	sum ++;
    	pthread_mutex_unlock(&mutex);
    }

    pthread_join(thread_id,NULL);
    printf("hilo principal sum=%d\n",sum );
    pthread_mutex_destroy(&mutex);


	return 0;
}

void * funcion_hilo (void *param)
{
	int i;
	printf("hilos %lu \n",pthread_self());
	for (int i = 0; i <10000; ++i)
	{
		pthread_mutex_lock(&mutex);
		sum++;
		pthread_mutex_unlock(&mutex);
		algo(16);
	}
	pthread_exit(0);
}
void algo(int n){
	usleep(n);
}