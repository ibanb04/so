#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define MAX_SIZE 1000

void *funcion_hilo(void * param);
void algo (int );
double MSE;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

int i;
double *a, *b;

typedef struct intervalo{
	int inicio, final;
}intervalo;

int main(){
    struct timespec tic, toc;
    double Elapsed;
    clock_gettime(CLOCK_REALTIME, &tic);

    int Numhilos= 20;
	pthread_t * thread_id;
	intervalo *Intervalo[2];
    MSE= 0;

    a = (double *) calloc (MAX_SIZE, sizeof(double));
    b = (double *) calloc (MAX_SIZE, sizeof(double));

    for(i=0; i<MAX_SIZE; i++){
        a[i]= (i*10)/(double)(i+1);
        b[i]= ((i+2)*10)/(double)(i+1);
    }

    /*for(i=0; i<MAX_SIZE; i++){
        printf("El valor de a en %d es: %f y el valor de b en %d es: %f \n", i, a[i], i, b[i]);
    }*/
    
    int parte= MAX_SIZE/Numhilos;

    printf ("Previa creacion de hilos MSE= %f\n", MSE);

    thread_id= (pthread_t*) calloc (Numhilos, sizeof (pthread_t));

    for(i=0; i<Numhilos; i++){
        Intervalo[i] = (intervalo*) malloc(sizeof(intervalo));
		Intervalo[i]->inicio = i*parte;
        
		Intervalo[i]->final = (Intervalo[i]->inicio + parte);
		pthread_create (&thread_id[i], NULL, funcion_hilo, (void*) Intervalo[i]);	
	}
	for(i=0; i<Numhilos; i++){
		pthread_join (thread_id[i], NULL);	
	}
	printf("Hilo principal MSE= %.51f\n", MSE);
	pthread_mutex_destroy(&mutex);

    clock_gettime(CLOCK_REALTIME, &toc);
    Elapsed = (toc.tv_sec-tic.tv_sec)+((toc.tv_nsec-tic.tv_nsec)/(double)1E9);
    printf ("%.6lf\n", Elapsed);

    return 0;
}

void * funcion_hilo(void *param){
	int j;
    int ini = ((intervalo*) param)-> inicio;
    int fin = ((intervalo*) param)-> final;
    for(j=ini;j<fin;j++){
        pthread_mutex_lock(&mutex);		
            //printf("El valor de a en %d es: %f y el valor de b en %d es: %f \n", j, a[j], j, b[j]);  
            MSE+= ((a[j]-b[j])*(a[j]-b[j]))/ MAX_SIZE;
		pthread_mutex_unlock(&mutex);    
    }
	pthread_exit(0);
}
