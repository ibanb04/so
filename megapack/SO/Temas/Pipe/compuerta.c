#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#define WR 1
#define RD 0

void *operacion(void *);

struct op_t{
	int op;
	int t;
};

int tb[5][2];

int main(){
	pthread_t hilos[5];
	int i;
	int descrip[5] = {0,1,2,3,4};
	int com;
	struct op_t *opt = (struct op_t *)malloc(sizeof(struct op_t));

	/******CREACION DE LAS TUBERIAS*******/
	for(i=0; i<5; i++){
		pipe(tb[i]);		
	}

	/*******CREACION DE LOS HILOS*******/
	for(i=0; i<5; i++){
		pthread_create(&hilos[i], NULL,operacion,	(void *)&descrip[i]);
	}

	while(1){
		printf("compuerta :");scanf("%d",&com);
		printf("opcion : ");scanf("%d",&opt->op);
		printf("tiempo: ");scanf("%d",&opt->t);

		write(tb[com][WR],opt,sizeof(struct op_t));
		usleep(200000);
	}

}

void * operacion(void *arg){

	int des = *((int *)arg) ;
	struct op_t *op = (struct op_t *)malloc(sizeof(struct op_t));
	while(1){
		read(tb[des][RD],op,sizeof(struct op_t));			
		printf("La compuerta %d se ",des);
		
		if(op->op==1){
			printf("cerrará en ");
		}
		else{
			printf("abrirá en ");
		}

		switch(op->t){
			case 0: printf("0 segundos\n");
						break;
			case 1:	printf("5 segundos\n");
						break;
			case 2: printf("10 segundos\n");
						break;
			default: break;
		}

	}
}
