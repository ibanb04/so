#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char const *argv[]){

	int fd[2],n;
	char buff[50];
	pipe(fd);
	if(fork()){
		close(fd[0]);
		write(fd[1], "Algun mensaje\n", 14);
		printf("[%d]-Escrito: Algun mensaje\n", getpid());
		wait(NULL);
	}else{
		close(fd[1]);
		n=read(fd[0], buff, 50);
		buff[n] = '\0';
		printf("[%d]-Leido: %s\n",getpid(), buff);
	}
	
	return 0;
}