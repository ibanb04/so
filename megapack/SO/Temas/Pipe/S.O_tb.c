#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char const *argv[]){
	
	int tb[2][2];
	pipe (tb[0]);
	pipe (tb[1]);

	char c [10];

	if (!fork()){

		close(tb[0][1]);
		close(tb[1][1]);
		close(tb[1][0]);

	 }else{
	 	if(!fork()){
	 		close(tb[0][0]);
	 		close(tb[0][1]);
	 		close(tb[1][1]);

	 	}else{
	 	close(tb[0][1]);
	 	close(tb[1][1]);

	 	sprintf(c,"pstree -lp %d", getpid());
	 	system(c);
	 	}
	 } 

	return 0;
}