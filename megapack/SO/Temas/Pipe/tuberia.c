#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>
#define max_tm_mj 80


int main()
{

	int pu,pi;
	int nHijos;
	int nTuberias;
	char buffer[max_tm_mj];
	char mj;
	printf("Ingrase el numero de hijos: \n");
	scanf("%d",&nHijos);
	printf("escriba el mensaje: \n");
	scanf("%c",&mj);
	fgets(buffer,max_tm_mj,stdin);
	printf("%c\n",mj);
	nTuberias=nHijos +1;
    int tub[nTuberias][2];
    pid_t hijos[nHijos],padre;
    padre= getpid();
    int contador;
	for(int i=0;i<nTuberias;i++){
		pipe(tub[i]);
	}
	
	//voy a crear las los hijos
	for(int j=0;j<nHijos;j++){
		hijos[j]=fork();
		if(hijos[j]==0){//es un hijo
			//cada hijo recorre las tuberias
			for(int x=0;x<nTuberias;x++){
				for(int z=0;z<2;z++){
					if(x==j && z==0){
						//el puesto por el que lee
						int n = read(tub[x][z],buffer,sizeof(buffer));
	                    printf("El proceso [%d] [%d] ha recibido: %s\n",j,getpid(),buffer);
					}else if(x==j+1 && z==0 ){
						//este es el puesto de escritura
						write(tub[x][z],buffer,sizeof(buffer));
						printf("El proceso [%d] [%d] ha enviado: %s\n",j	,getpid(),buffer);
					}else{
						close(tub[x][z]);
					}

				}
			}

			break;
		}else{//es el padre

			//recorremosla matrix de tuberias
			for(int k =0;k<nTuberias;k++){
				for (int l = 0; l < 2; l++){
					if(k==0 && l==0){
						close(tub[k][l]);
						//int n = read(tub[k][l],buffer,sizeof(buffer));
	                    //printf("El proceso padre: [%d] ha recibido %s\n",getpid(),buffer);
					}else if(k==0 && l==1){
						//envia 
						write(tub[k][l],buffer,sizeof(buffer));;
						printf("El proceso padre: [%d] ha enviado: %s\n",getpid(),buffer);
					}else{
						if(k==nTuberias-1 && l==nTuberias-2){
							pu=k;
							pi=l;
						}else{
						close(tub[k][l]);}
					}

				}
			}
			for(int r=0;r<nHijos;r++){
				wait(NULL);
			}
			/*if(pu==nTuberias-1 && pi==nTuberias-2){
							int n = read(tub[pu][pi],buffer,sizeof(buffer));
	                        printf("El proceso padre: [%d] ha recibido %s\n",getpid(),buffer);
			}*/

		}
	}
	
 	


	return 0;
}