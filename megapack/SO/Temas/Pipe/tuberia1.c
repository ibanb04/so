#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

int main(){
	int *tb = NULL,i;
	char mensaje[50];
	int rd, wr;
	for(i=0; i<10; i++){
		tb = (int *)calloc(2,sizeof(int));		
		pipe(tb);
		if(fork()==0){
			if(i==0){
				close(tb[1]);
				rd = tb[0];
				read(rd,mensaje,20);
				printf("---- %s proceso[%d] padre[%d] ---> %d\n",mensaje,getpid(),getppid(),i);
			}
			else{
				close(rd);
				close(tb[1]);
				rd = tb[0];
				printf("---- %s proceso[%d] padre[%d] ---> %d\n",mensaje,getpid(),getppid(),i);
			}
			
		}    
		else{
			close(tb[0]);
			wr = tb[1];
			write(wr,"Pasalo",20);
			break;
		}
	}
	free(tb);
	wait(NULL);

return 0;
}
