#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

/*Este codigo se basa en separar las funciones genrales de cualquier padre y cualquier hijo*/
/* lean los comentarios primero y luego el codigo :D */
int main(){
    int p[2];		/*Vector que se usa para pasar por parámetro, en la funcion pipe(int *argumento)*/
	int i,n;		/*contador para el for: (i) y numero de procesos :(n)*/
	int p_read;		/*variable que almacena el descrptor para la lecura de cualquier proceso*/ 
	int p_write;	/*variable que almacena el descrptor para la escrutura de cualquier proceso*/
	
	int padre = 0;	/*Bandera que identifica a un proceso cualuiera como padre, 
				  	si su valor es uno en un proceso A quiere decir que ese proceso es padre de alguien*/
	
	int pid_hijo;	/*Si un proceso es padre... esta variable tendra el id de su hijo*/
	
	int hijo = 0;	/*Bandera que identifica a un proceso cualquiera como hijo,
				 	si su valor es uno en un proceso A quiere decir que ese proceso es hijo de alguien*/
    
	char buffer[50];/*Mensaje que se le comunica a atravez de la tuberia*/
    printf("Ingrese la cantidad de procesos: ");
    scanf("%d",&n);
    pipe(p);/*Se cea la tuberia del primer proceso*/

	/*------Codigo para crear la estructura general-------*/    
	for(i = 0;i < n;i++){
        if(!(pid_hijo = fork())){/*si (eres un hijo)*/
            p_read = p[0];/*guarde el descriptor de lectura*/
            close(p[1]);/*cierre el descriptor de escritura*/
            if((i+1)!=n)/*si no es el ultimo hijo... significa que serás padre asi que....*/
                pipe(p);/*Cree otra tuberia para tu proximo hijo*/
            hijo = 1;/*Identificate como hijo*/
            padre = 0;/*Identificate como alguien que no tiene hijos, (no ser padre)*/
        }else{/*Si no... eres un padre*/
            close(p[0]);/*cierra del descriptor de lectura*/
            p_write = p[1];/*guarda el descriptor de escritura*/
            padre = 1;/*guarda el descriptor de escritura*/
            break;/*no necesitas crear mas hijos asi que sal del for*/
        }
    }
    /*------Codigo para crear la estructura general-------*/

	/*----------codigo de prueba--------------*/

    if(padre == 1){/*Si eres un padre*/
        write(p_write,"Soy tu padre",strlen("Soy tu padre"));/*Comunica el mensaje a tu hijo*/
        printf("Soy[%d] y le dije a mi hijo[%d] \"Soy tu padre\"\n",getpid(),pid_hijo);/*dime quien eres y que mensaje mandaste*/
        wait(NULL);/*espera que tu hijo haga sus funciones*/
    }
    
	if(hijo == 1){/*si eres un hijo*/
        n = read(p_read,buffer,50);/*lee el mensaje que de mandó tu padre*/
        buffer[n]='\0';/*formatea el mensaje para que se vea bien*/
        printf("Soy el hijo[%d] y mi padre[%d] me dijo \"%s\"\n",getpid(),getppid(),buffer);/*identificate, 
		dime quien es tu padre y cual fue el mensaje mandó*/
    }

	/*----------codigo de prueba--------------*/

	/*noten que hay hijos que son padres al mismo tiempo.... 
	por eso en la anterior sentencia se ponen dos if sucesivos en ves de if... else...*/
}
