#include <stdio.h>
#include <unistd.h>
#include <sys/wart.h>
#include <signal.h>

void manejador(int s){}

void mkill(pid_t, int);

int main(){
	pid_t hijos[2], Padre;
	int c;
	signal (SIGUSR1, manejador);
	Padre-getpid();
	for(c=0;c<2;c++){
		hijos[c] = fork();
		if(!hijos[c]) break;
	}
	if(padre==getpid()){
		printf(" %d  %d\n", getpid(), getppid());
		mkill(hijos[c], SIGUSR1);
		pause();
		printf(" %d  %d\n", getpid(), getppid()); wait(NULL), wait(NULL);
		pause();
	}else{
		printf(" %d  %d\n", getpid(), getppid());
		if(c){
			mkill(hijos[c-1], SIGUSR1);
		}else{
			mkill(getppid(), SIGUSR1);
		}
	}
	return 0;
}
void mkill(pid_t pid, int s){
	usleep(1000);
	kill(pid, s);
}