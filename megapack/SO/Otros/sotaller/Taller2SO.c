 #include <stdio.h>
#include <unistd.h>
#include <wait.h>
#include <sys/shm.h>
#include <sys/stat.h>



unsigned int sizeof_dm(int rows, int col, size_t sizeElement){
	size_t size = rows*sizeof(void *);
	size += (rows*col*sizeElement); 
	return size;
}

void create_index(void **m, int rows, int cols, size_t sizeElement){

	int i;
	size_t sizeRow = cols*sizeElement;
	m[0]= m+rows;
	for(i=1; i<cols; i++){
		m[i] = (m[i-1]+sizeRow);
	}
}

int main(){

int Rows=3;
int Cols=3;
int shmId;
int x,y; 
double **matrix;

size_t sizeMatrix = sizeof_dm(Rows,Cols,sizeof(double));
shmId = shmget(IPC_PRIVATE, sizeMatrix, IPC_CREAT|0600);
matrix = shmat(shmId, NULL, 0); 
create_index((void*)matrix, Rows, Cols, sizeof(double));

printf("la matriz original es:\n \n");
for( x=0; x<Cols; x++){
  for( y=0; y<Rows; y++){
     matrix[x][y]=((double)(x+y));
     printf(" | %f |\t",matrix[x][y]);
  }
  printf("\n");
} 
 printf("\n\n");

if(!fork()){

	matrix[0][2]= 5; 
	matrix[1][1]= 6;
	matrix[2][0]= 8.5;
	shmdt(matrix);

}
else{
wait(NULL); 
printf("Imprimiendo desde el padre,el resultado es: \n \n");
for( x=0; x<Cols; x++){
  for( y=0; y<Rows; y++){
     
     printf(" | %f |\t",matrix[x][y]);
  }
  printf("\n");
} 
shmdt(matrix);
shmctl(shmId,IPC_RMID,0);
}
return 0; 
}



