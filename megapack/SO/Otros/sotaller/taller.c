#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <wait.h>


int main(){

double *a, *b;
int MAX_SIZE = 0;

double suma(int lini, int lfin){
	int d;
	double suma=0;
	for(d=lini; d<=lfin; d++){
		suma +=(a[d]-b[d])*(a[d]-b[d]);			
	}
	return	suma /= MAX_SIZE;
}


	int nh = 0;
	
	printf("Ingrese la cantidad maxima de valores para los vectores \n");
	scanf("%d",&MAX_SIZE);
			/*printf("num es%d",MAX_SIZE);*/

	printf("Ingrese la cantidad de procesos que desea crear (Hijos) \n");
	scanf("%d",&nh);
			/*printf("num es%d",nh);*/
	
	a=(double *) calloc(MAX_SIZE, sizeof(double));
	b=(double *) calloc(MAX_SIZE, sizeof(double));

	for (int i=0; i<MAX_SIZE ; i++){
		a[i]=i/(double)(i+1);
		b[i]=(i+1)/(double)(i+2);
	}

struct timespec tic, toc;
double Elapsed;

	int fd[2],n;
		double sum[nh], sum2=0.0; 
	pipe(fd);

	int div;
	div = MAX_SIZE/nh;
	int lini, lfin,k;
	
	int padre = getpid();

	int i=0;
	double sumtotal;
clock_gettime(CLOCK_REALTIME, &tic);
	for (i=0; i<nh; i++){
		lini = i * div ;
		lfin = lini+div-1;
			/*printf("\nini%d \n",lini);
			printf("\nfins%d \n",lfin);*/
	
		if(!fork()){
		close (fd[0]);	
		sum[i] = suma(lini,lfin);
		/*printf("\nvalor%.61f \n",sum[i]);*/
			write(fd[1], &sum[i], sizeof(double));
			break;			
		}		

	}

		if(i==nh){
		close(fd[1]);
		for(k=0; k<nh; k++){
			n=read(fd[0], &sum[k], sizeof(double));
			wait(NULL);
		sum2+= sum[k];
		}
		printf("La suma es: = %.20f\n", sum2);
clock_gettime(CLOCK_REALTIME, &toc);
		Elapsed = (toc.tv_sec-tic.tv_sec)+((toc.tv_nsec-tic.tv_nsec)/(double)1E9);
		printf("Tiempo: %.10f\n", Elapsed);
}
	
return 0;
}
