#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>
#include <time.h>

struct datos{
	int *a,*b,*c,*d;
	int size;
};

void * f(void*arg);

pthread_t h[4];
struct datos dt;

int main(){
	
	int i;
	printf("ingrese el tama�o de los vectores : ");
	scanf("%d",&dt.size);
	
	dt.a = (int*) calloc(dt.size, sizeof(int));
    dt.b = (int*) calloc(dt.size, sizeof(int));
	dt.c = (int*) calloc(dt.size, sizeof(int));
	dt.d = (int*) calloc(dt.size, sizeof(int));
		
	srand(time(NULL));
    for(i=0; i<dt.size; i++){    
        dt.a[i]= rand()%10;
        dt.b[i]= rand()%10;
    }
    	

	int *l;
    for ( i = 0; i < 3; i++){
        l = (int*)malloc(sizeof(int)); 
        *l = i;
        pthread_create(&h[i], NULL, f, (void*)l);
    }
    
    pthread_join(h[2],NULL);
	
	return 0;
}

void imprimir(){
	int i;
	printf("\n________________________________________________\n");
     	printf("\t c = a * b\n[");
     	for( i=0; i<dt.size; i++){
     		printf(" %d",dt.c[i]);
	    }
     	printf("] = [");
     	for( i=0; i<dt.size; i++){
     		printf(" %d",dt.a[i]);
	    }
	    printf("] ]* [");
     	for( i=0; i<dt.size; i++){
     		printf(" %d",dt.b[i]);
	    }
	    printf("]\n");
	    printf("________________________________________________\n");
	    printf("\t d = c * c\n[");
     	for( i=0; i<dt.size; i++){
     		printf(" %d",dt.d[i]);
	    }
     	printf("] = [");
     	for( i=0; i<dt.size; i++){
     		printf(" %d",dt.c[i]);
	    }
	    printf("] ]* [");
     	for( i=0; i<dt.size; i++){
     		printf(" %d",dt.c[i]);
	    }
	    printf("]\n");
}

 
void * f(void*arg){
	
	int index = *(int*)arg;
    int i;
    
     if(index==0){
        for( i=0; i<dt.size; i++){
            dt.c[i] = dt.a[i] * dt.b[i];
		} 	
	 }
	 else if(index==1){
	 	pthread_join(h[0],NULL);
     	for( i=0; i<dt.size; i++){
            dt.d[i] = dt.c[i] * dt.c[i];
		} 
	 }
	 else if(index==2){
     	pthread_join(h[1],NULL);
     	imprimir();
	 }
     
    return NULL; 	
}



