#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>

pthread_t *vpt;
int pos=0;
int posx=0;
int posy=0;

typedef struct data{
    int n; //tamano de la matriz
    int i; //numero del hilo
    int c; //numero de posiciones contiguas a calcular
    int k; //numero de hilos total
}data;

double **mat1,**mat2,**mat3;
void * thread_handler(void *);
int condition(int);
void * thread_handler_mat3(void *);
double ** create_matrix(int );
void fill_matrix_rand(double**,int );
void fill_matrix_empty(double**,int );
void print_matrix(double**,int);
void free_matrix_memory(double**, int);
void operate_position(int,int,int);


pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;


int main(){
    srand(time(NULL));
    int i,n, k,c;
    printf("Ingrese el tamaño de las matrices a utilizar: \n");
    //scanf("%d", &n);
    n=3;
    printf("Ingrese el valor de k: \n");
    //scanf("%d", &k);
    k=5;
    printf("Ingrese el valor de c: \n");
    //scanf("%d", &c);
    c=1;
    mat1 = create_matrix(n);
    fill_matrix_rand(mat1,n);
    mat2 = create_matrix(n);
    fill_matrix_rand(mat2,n);
    mat3 = create_matrix(n);
    fill_matrix_empty(mat3,n);
    print_matrix(mat1,n);
    print_matrix(mat2,n);
    data * vector_data[k+1];
    vpt = (pthread_t *) calloc(k+1, sizeof(pthread_t));
    
    for(i=0; i<k; i++){
        vector_data[i] = (data *) malloc(sizeof(data));
        vector_data[i]->n=n;
        vector_data[i]->c=c;
        vector_data[i]->i=i;
        pthread_create(&vpt[i],NULL,thread_handler,vector_data[i]);
    }
    vector_data[k] = (data *) malloc(sizeof(data));
    vector_data[k] ->n = n;
    vector_data[k] ->k = k;
    vector_data[k] ->i = k;
    pthread_create(&vpt[k],NULL,thread_handler_mat3, vector_data[k]);
    pthread_join(vpt[k],NULL);
    free_matrix_memory(mat1,n);
    free_matrix_memory(mat2,n);
    free_matrix_memory(mat3,n);
    //free(vector_data);
    //free(vpt);
    return 0;
}

void* thread_handler(void * param){
    int c= ((data *) param)->c;
    int n = ((data *) param)->n;
    int id = ((data*) param )->i;
    int i;
    int pi=-1;
    int pf=-1;
    while(condition(n)){
        pthread_mutex_lock(&mutex);
            if(pos<n*n){
                pi=pos;
                pos+=c;
                pf=pos;
            }
        pthread_mutex_unlock(&mutex);
        printf("id: [%d] calculando...\n", id);
        for(i=pi; i<pf&&i<n*n; i++){
            //printf("[%d][%d]\n", i/n, i%n);
            operate_position(n, i/n, i%n);
        }
        
    }
    
}

int condition(int n){
    int sw=0;
    pthread_mutex_lock(&mutex);
    if(pos<n*n){
        sw=1;
    }
    pthread_mutex_unlock(&mutex);
    return sw;
}

void * thread_handler_mat3(void* param){
    
    int n = ((data *) param)->n;
    int k = ((data *) param)->k;
    int i;
    for(i=0; i<k; i++){
        pthread_join(vpt[i], NULL);
    }
    print_matrix(mat3,n);
}

void fill_matrix_empty(double **mat,int n){
    int i, k;
    for(i=0; i<n; i++){
        for(k=0; k<n; k++){
            mat[i][k]= 0;
        }
    }
}

double** create_matrix(int n){
    int i;
    double ** matrix;
    matrix = (double **)  calloc(n, sizeof(double*));
    for(i=0; i<n; i++){
        matrix[i] = (double *)  calloc(n, sizeof(double*));
    }
    return matrix;
}

void fill_matrix_rand(double **mat,int n){
    int i, k;
    for(i=0; i<n; i++){
        for(k=0; k<n; k++){
            mat[i][k]= rand()%10;
        }
    }
}

void print_matrix(double **mat,int n){
    int i, k;
    for(i=0; i<n; i++){
        for(k=0; k<n; k++){
            printf("[%.1f]", mat[i][k]);
        }
        printf("\n");
    }
    printf("\n");
}

void free_matrix_memory(double **mat,int n){
    int i;
    for(i=0; i<n; i++){
        free(mat[i]);
    }
    free(mat);
}

void operate_position(int n,int i, int k){
    int p;
    for(p=0; p<n; p++){
        mat3[i][k] += mat1[i][p]*mat2[p][k];
    }
}
