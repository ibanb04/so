#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"
#include "pthread.h"

int **MA,**MB,**MR;  // declaracion de las matrices
pthread_t *h; // vector que contendra los identificadores de los hilos
int M,K,C;  // variables a utilizar


void * f(void*arg);  // funcion que ejecutan los hilos

int main(int argc, char const *argv[])
{
	
    int i,j,*datos;   // variables a utilizar


    printf("Ingrese  el tama� de la matriz M : ");
    scanf("%d",&M);

    printf("Ingrese ingrese la cantidad de hilos K : ");
    scanf("%d",&K);

    printf("ingrese la cantidad de posiones por hilo C : ");
    scanf("%d",&C);

    // inicializacion de las matrices dinamicamente 
    MA = (int**)calloc( M , sizeof(int*) );
    MB = (int**)calloc( M , sizeof(int*) );
    MR = (int**)calloc( M , sizeof(int*) );

    for(i=0; i<M; i++){
	    MA[i] =  (int*)calloc( M , sizeof(int) );
	    MB[i] =  (int*)calloc( M , sizeof(int) );
	    MR[i] =  (int*)calloc( M , sizeof(int) );
    }
    ////////////////////////////////////////////////
    

    ///  llena las matrices /////////7
    int g=1;
    for ( i = 0; i < M; i++ )
	{
		for ( j = 0; j < M; j++ )
	    {
	    
	         MA[i][j] =  g;
	         MB[i][j] =  g;
			 g++; 
		}
	}
    ///////////////////////////
   
    /// inicializacion del vector   de ilos con K+1 posiciones
	h = (pthread_t*)calloc( K+1 , sizeof(pthread_t) );
	///////////////////////////////////////////////////////
	
	
   // creacion de K+1 hilos ////////////////
	for ( i = 0; i < (K+1); i++ )
	{
		datos = (int*)calloc(2,sizeof(int)); //  inicializacion de un vector de dos pociones para enviarselo a  cada hilo
  		datos[0] = i*C;
	    datos[1] = (i+1)*C;  
		
		pthread_create(&h[i],NULL,f,datos);
	}

	pthread_join(h[K],NULL);
    
	return 0;
}

void * f(void*arg){
    
    int *index = (int*)arg; // casteo el vector que 
    int i,j,k,pos=0;  //   varibles a utilizar
    if( (index[0]/C) < K ){
        
    	for( i=0; i<M; i++){
          for( j=0; j<M; j++){
          	 if( pos>=index[0] && pos<index[1] ){			   
	             MR[i][j] = 0;
	             for( k=0; k<M; k++){ MR[i][j] = MR[i][j] +( MA[i][k] * MB[k][j] ); }   
	         }
	           pos++;
          }
       }
    	
	}
    else{
        
        
    	for ( i = 0; i < (K-1); i++ )
		{
			pthread_join(h[i],NULL);
	    }

        
	   printf("\n");
	    for ( i = 0; i < M; i++ )
		{
			for ( j = 0; j < M; j++ )
		    {
	            printf("\t%d",MR[i][j]);
		    }
		    printf("\n");
		}
    	
	}
	
	return NULL;
}



