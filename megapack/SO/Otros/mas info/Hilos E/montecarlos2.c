#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#define MAX_HILOS 512

void* calcular_pi (void*);
void* principal (void*);

int hits[MAX_HILOS],num,num_puntos;
double pi=0.0;
pthread_t p_threads[MAX_HILOS];

int main() {
        int i,num_hilos;
	printf("numero de puntos\n");
	scanf("%d",&num_puntos);
	printf("numero de hilos\n");
	scanf("%d",&num_hilos);
	num = num_puntos/num_hilos;
  for (i=0; i<num_hilos; i++) {
	hits[i] = i;
	pthread_create(&p_threads[i],NULL,principal,(void *) &hits[i]);
  }
  for (i=0; i<num_hilos; i++) {
	pthread_join(p_threads[i], NULL);
  }
  printf("%4f",pi);
  return 0;
}
void* principal (void*s) {
	int i;
	int *numero;
	numero = (int*) s;
        double rand_no_x, rand_no_y;
	int local_hits;
	local_hits = 0;
        srand(time(NULL));
  for (i = 0; i<num; i++) {
	rand_no_x = (double)rand()/(double) RAND_MAX;
	rand_no_y = (double)rand()/(double) RAND_MAX;
	if (((rand_no_x) * (rand_no_x) + (rand_no_y) * (rand_no_y)) <= 1.0)
	local_hits ++;
  }
       pthread_create(&p_threads[*numero],NULL,calcular_pi,(void*)local_hits);

       for (i=0;i<*numero;i++){
       pthread_join(p_threads[i],NULL);
        }
	pthread_exit(0);
}
void* calcular_pi (void*s){
     int contador;
     contador += (int)s; 
     pi = pi + (4.0*((double)contador))/(double)num_puntos;
     pthread_exit(0);
}
