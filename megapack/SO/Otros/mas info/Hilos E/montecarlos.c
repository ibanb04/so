#include "stdio.h"
#include "stdlib.h"
#include "pthread.h"
#include "math.h"

int turno=0,sw=0,ne;
int conp=0;
pthread_mutex_t mutex= PTHREAD_MUTEX_INITIALIZER;
void *run(void*arg){
	int i;
	double x,y;
	srand(time(NULL));
	for ( i = 0; i < ne; i++)
	{
		x=drand48();
		y=drand48();
		if((x*x)+(y*y)<=1)
		{
			pthread_mutex_lock(&mutex);
			conp++;
			pthread_mutex_unlock(&mutex);

		}
	}
	
	return NULL;
}
int main(int argc, char const *argv[])
{
	int i,*a,nh,np;
	double pi;
	pthread_t *hilo;

	printf("Ingrese el numero de puntos\n");
	scanf("%d",&np);
	printf("Ingrese el numero ejecucion para cada hilo\n");
	scanf("%d",&ne);

	nh=np/ne;
	hilo=(pthread_t*)malloc(sizeof(pthread_t)*nh);
	
	for ( i = 0; i < nh; i++)
	{
		pthread_create(&hilo[i],NULL,run,NULL);
	}
	for ( i = 0; i < nh; i++)
	{
		pthread_join(hilo[i],NULL);
	}
	pi=4.0*((double)conp/(double)np);
	printf("El valor es%f\n",pi );
	return 0;
}