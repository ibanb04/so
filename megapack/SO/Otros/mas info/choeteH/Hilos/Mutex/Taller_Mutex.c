#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

struct nodo{
    int hijo;   
    };
void *funcion_hilo(void *);
void algo(int );
int global=0,inter,aux1=0;
float pi,suma=0;
double delta;

pthread_mutex_t mutex= PTHREAD_MUTEX_INITIALIZER;


int main(){

	int i,x;
	pthread_t hilos[100];
	struct nodo *Nodo;
	printf("Ingrese el valor de Hilos utilizar: \n");
	scanf("%d",&x);
	printf("Ingrese el valor de Intervalos a calcular: \n");
	scanf("%d",&inter);	
	delta=1/(double)inter;	
																																																										
	for (i = 0; i < x; i++)
	{
		Nodo=(struct nodo *)malloc(sizeof(struct nodo));    
    	Nodo->hijo=i;
		pthread_create(&hilos[i],NULL,funcion_hilo,(void*)Nodo);		
	}

	;
	for (i = 0; i < x; i++)
	{
		pthread_join(hilos[i],NULL);	
	}
	
	pthread_mutex_destroy(&mutex);
	pi=suma*delta;
	printf("Pi= %.10f\n",pi);	
	return 0;
}

void * funcion_hilo(void *param){
	double y,z;
	struct nodo *func=((struct nodo*)param);
	
	while(aux1<inter){
		if(global==0){
		pthread_mutex_lock(&mutex);
		global=1;
		z=(double)(aux1-0.5)*delta;
		y=4.0/(1.0 +(z*z));
		suma+=y;
		aux1++;
		global=0;
		pthread_mutex_unlock(&mutex);
	}}

	pthread_exit(0);
}

