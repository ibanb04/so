#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

#define CHILOS 20

void *funcion_hilo(void *);
void algo(int );
int global=0;
pthread_mutex_t mutex= PTHREAD_MUTEX_INITIALIZER;


int main(){

	int i;
	pthread_t hilos[CHILOS];
	
	for (i = 0; i < CHILOS; i++)
	{
		pthread_create(&hilos[i],NULL,funcion_hilo,NULL);		
	}

	;
	for (i = 0; i < CHILOS; i++)
	{
		pthread_join(hilos[i],NULL);	
	}
	
	pthread_mutex_destroy(&mutex);	
	return 0;
}

void * funcion_hilo(void *param){

	int i;
	algo(global);	

	pthread_mutex_lock(&mutex);	
	global++;
	printf("%d\n",global);
	pthread_mutex_unlock(&mutex);


	
	pthread_exit(0);
}

void algo(int n){
	usleep(10000);
}