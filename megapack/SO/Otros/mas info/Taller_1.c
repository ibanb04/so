#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>

typedef struct Datos{
    pthread_t *pt;
    int i;
    int k;
    int c;
    int n;
}Datos;

int pos = 0;

double **m1,**m2,**m3;
void * manejador_hilos(void *);
void * manejador_hilos2(void *);
double ** crear_matriz(int );
void llenar_matriz(double**,int );
void llenar_matriz3(double**,int );
void borrar_matriz(double**,int );
void calcular_posicion(int n,int i, int j);
void imprimir_matriz(double**,int);

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

int main(){
    srand(time(NULL));
    int n = 0,k = 0, c = 0,i,j;
    printf("Ingrese el tamano de la matriz: ");
    scanf("%d",&n);

    m1 = crear_matriz(n);
    m2 = crear_matriz(n);
    m3 = crear_matriz(n);

    llenar_matriz(m1,n);
    llenar_matriz(m2,n);
    llenar_matriz3(m3,n);

    printf("Ingrese la cantidad de hilos: ");
    scanf("%d",&k);

    Datos *nodos[k+1];

    printf("Ingrese el valor de c: ");
    scanf("%d",&c);

    printf("Matriz 1:");
    imprimir_matriz(m1,n);
    printf("\nMatriz 2:");
    imprimir_matriz(m2,n);

	pthread_t hilos[k+1];
	for(i = 0;i<k;i++){
        nodos[i] = (Datos *)malloc(sizeof(Datos));
        nodos[i]->c = c;
        nodos[i]->k = k;
        nodos[i]->n = n;
        nodos[i]->i = i;
        pthread_create(&hilos[i],NULL,manejador_hilos,nodos[i]);
	}
	nodos[k] = (Datos*)malloc(sizeof(Datos));
    nodos[k]->c = c;
    nodos[k]->k = k;
    nodos[k]->n = n;
    nodos[k]->i = i;
    nodos[k]->pt = hilos;
    pthread_create(&hilos[k],NULL,manejador_hilos2,nodos[k]);

    pthread_join(hilos[k],NULL);

    for(i = 0;i<k+1;i++){
        free(nodos[i]);
    }

    borrar_matriz(m1,n);
    borrar_matriz(m2,n);
    borrar_matriz(m3,n);

    return 0;
}

void llenar_matriz3(double **m,int n){
    int i,j;
    for(i = 0;i<n;i++){
        for(j = 0;j<n;j++){
            m3[i][j] = 0;
        }
    }
}

double** crear_matriz(int n){
    double **m = (double**) calloc(n,sizeof(double*));
    int i;
    for(i = 0;i<n;i++){
        m[i] = (double *) calloc(n,sizeof(double));
    }
    return m;
}

void llenar_matriz(double **m,int n){
    int i,j;
    for(i = 0;i<n;i++){
        for(j = 0;j<n;j++){
            m[i][j] = rand()%6;
        }
    }
}

void imprimir_matriz(double **m,int n){
    int i,j;
    for(i = 0;i<n;i++){
        printf("\n");
        for(j = 0;j<n;j++){
            printf("[%.1f]",m[i][j]);
        }
    }
}

void borrar_matriz(double **m,int n){
    int i;
    for(i = 0;i<n;i++){
        free(m[i]);
    }
    free(m);
}

int condicion(int n){
    int cond = 0;
    pthread_mutex_lock(&mutex);
    cond = pos < n*n;
    pthread_mutex_unlock(&mutex);
    return cond;
}

void* manejador_hilos(void * param){
    int n = ((Datos*)param)->n;
    int c = ((Datos*)param)->c;
    while(condicion(n)){
        int p = -1;
        pthread_mutex_lock(&mutex);
        if(pos < n*n){
            p = pos;
            pos += c;
        }
        pthread_mutex_unlock(&mutex);
        if(p>=0){
            int ps = 0;
            for(ps = p;ps<p+c&&ps<n*n;ps++){
                int i = ps/n;
                int j = ps%n;
                calcular_posicion(n,i,j);
            }
        }
    }
}

void * manejador_hilos2(void* param){
    int i,j;
    pthread_t *pt = ((Datos*)param)->pt;
    for(i = 0;i<((Datos*)param)->k;i++){
        pthread_join(pt[i],NULL);
    }
    printf("\nMatriz 3:");
    for(i = 0;i<((Datos*)param)->n;i++){
        printf("\n");
        for(j = 0;j<((Datos*)param)->n;j++){
            printf("[%.1f]",m3[i][j]);
        }
    }
    printf("\nFin hilo %d",((Datos*)param)->k+1);
}

void calcular_posicion(int n,int i, int j){
    int k = 0;
    for(k = 0;k<n;k++){
        m3[i][j] += m1[i][k]*m2[k][j];
    }
}
