#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <wait.h>
#include <stdlib.h>

int main(){
int tb1[2],tb2[2],tb3[2],tb4[2];
char cadena[100];
pipe(tb1);
pipe(tb2);
pipe(tb3);
pipe(tb4);

if(fork()==0){

  
  if(fork()==0){
	close(tb1[0]);
	close(tb1[1]);
	close(tb2[1]);
	close(tb3[0]);
	close(tb4[1]);
	close(tb4[0]);
        read(tb2[0],&cadena,sizeof(cadena));
	printf("\n");	
	printf("soy el hijo3 imprimiendo:%s\n",cadena);
	printf("soy el hijo 3 id: %d\n",getpid());		 
        write(tb3[1],&cadena,sizeof(cadena));          		 

		 }else{
	close(tb1[1]);
	close(tb2[0]);
	close(tb3[1]);
	close(tb4[0]);
        read(tb1[0],&cadena,sizeof(cadena));
        printf("soy el hijo 2 imprimiendo:%s\n",cadena);
	printf("soy el hijo 2 id:%d\n",getpid());
        write(tb2[1],&cadena,sizeof(cadena));
        read(tb3[0],&cadena,sizeof(cadena));
	printf("soy el hijo 2 resiviendo la cadena de 3:%s\n",cadena);
        printf("soy el hijo 2 id de regreso:%d\n",getpid());
        write(tb4[1],&cadena,sizeof(cadena));	 
	}

}else{
 
	close(tb1[0]);
	close(tb2[0]);
	close(tb2[1]);
	close(tb3[0]);
	close(tb3[1]);
	close(tb4[1]);
        printf("Escribe el texto");
	fgets(cadena, 100, stdin);
	//strcpy( cadena, "Roma" ); 
        printf("soy el proceso padre enviando el texto:%s\n",cadena);
	printf("soy el id del proceso padre:%d\n",getpid());
        write(tb1[1],&cadena,sizeof(cadena));
        read(tb4[0],&cadena,sizeof(cadena));  
        printf("soy el padre resiviendo del hijo 2:%s\n",cadena);
	printf("soy el id padre:%d\n",getpid());
        }




}
