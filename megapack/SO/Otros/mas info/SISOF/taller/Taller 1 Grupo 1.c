#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <wait.h>
#include <stdlib.h>


int main()
{
int tb1[2],tb2[2],tb3[2],tb4[2];   
pipe(tb1);
pipe(tb2);
pipe(tb3);
pipe(tb4);
int vector[10];

if(fork()==0){
  close(tb1[1]);
  close(tb2[0]);
  close(tb3[0]);
  close(tb3[1]);
  close(tb4[0]);
  close(tb4[1]);
  read(tb1[0],(void*)vector,sizeof(vector));  
	int i,suma;
	float promedio;
	
	for(i=0;i<10;i++){
		suma=suma+vector[i];
	}
	
	promedio=((float)(suma)/(float)10);
	write(tb2[1],vector,sizeof(vector));
	write(tb2[1],&promedio,sizeof(promedio));
	

    
}else if(fork()==0){
    close(tb1[0]);
    close(tb1[1]);
    close(tb2[1]);
    close(tb3[0]);
    close(tb4[0]);
    close(tb4[1]);
    
    float promedio;
    read(tb2[0],vector,sizeof(vector));
    read(tb2[0],&promedio,sizeof(promedio));
    
    
    int i;
	float Varianza;
	float Temporal,Elevado,dividendo;
    for(i=0;i<10;i++){
    Temporal=(vector[i]-(promedio));    
	Elevado=Temporal*Temporal;    
    dividendo=dividendo+Elevado;  
    }
    Varianza=dividendo/10;
 // printf("\nSoy varianza %f",Varianza);
    write(tb3[1],&Varianza,sizeof(Varianza));
    write(tb3[1],&promedio,sizeof(promedio));
    
    
    
}else if(fork()==0){
    close(tb1[0]);
    close(tb1[1]);
    close(tb2[0]);
    close(tb2[1]);
    close(tb3[1]);
    close(tb4[0]);
    
    float Varianza,DesviacionE,promedio;
    read(tb3[0],&Varianza,sizeof(Varianza));
    read(tb3[0],&promedio,sizeof(promedio));
    //DesviacionE=sqrt(Varianza);
    
    write(tb4[1],&Varianza,sizeof(Varianza));
    write(tb4[1],&DesviacionE,sizeof(DesviacionE));
    write(tb4[1],&promedio,sizeof(promedio));
}else{
	close(tb1[0]);
	close(tb2[0]);    
	close(tb2[1]);
	close(tb3[0]);    
	close(tb3[1]);
	close(tb4[1]);
	    
	printf("Ingrese numeros");
	int i;

	for(i=0;i<10;i++){
		printf("Vector[%d]:",i);
		scanf("%d",&vector[i]);    
   }
   write(tb1[1],(void*)vector,sizeof(vector));
   wait(NULL); 
   float Varianza,DesviacionE,promedio;
    
    read(tb4[0],&Varianza,sizeof(Varianza));
    read(tb4[0],&DesviacionE,sizeof(DesviacionE));
    read(tb4[0],&promedio,sizeof(promedio));
    printf("\nSoy la Varianza:%f",Varianza);
    printf("\nSoy la Desviacion estandar:%f",DesviacionE);
    printf("\nSoy el promedio:%f",promedio);
}

    return 0;
}


