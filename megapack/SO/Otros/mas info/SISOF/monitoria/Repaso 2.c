#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
//lectura   0
//escritura 1

int main(){
    
    int tb1[2],tb2[2],tb3[2],tb4[2];
    pipe(tb1);
    pipe(tb2);
    pipe(tb3);
    pipe(tb4);

    if(fork()==0){
        int vector[2];  
        int suma=0;
        close(tb1[1]);
        close(tb2[0]);
        close(tb3[1]);
        close(tb3[0]);
        close(tb4[1]);
        close(tb4[0]);
        read(tb1[0],vector,sizeof(vector));
        
        suma=vector[0]+vector[1];
        printf("\nsoy el hijo mandando la suma\n");
        write(tb2[1],&suma,sizeof(suma));

    }else if(fork()==0){
        //soy el hijo 2
       close(tb1[1]);
       close(tb1[0]);
       close(tb2[1]);
       close(tb2[0]);
       close(tb3[1]);
       close(tb4[0]);
       int vector[2];
      read(tb3[0],vector,sizeof(vector)); 
      
      
        
        
    }else{
        close(tb1[0]);
        close(tb2[1]);
        close(tb3[0]);
        close(tb4[1]);
        int n1,n2;
        	
        printf("Ingrese el numero n1:");
        scanf("%d",&n1);
        printf("Ingrese el numero n2:");
        scanf("%d",&n2);
        int vector[2]={n1,n2};
        
        write(tb1[1],vector,sizeof(vector));
        wait(NULL);
        int sumap;
        read(tb2[0],&sumap,sizeof(sumap));
        printf("Suma: %d\n",sumap);
        printf("\n");
        write(tb3[1],vector,sizeof(vector));
        wait(NULL);
        int restap;
        read(tb4[0],&restap,sizeof(restap));
        printf("Resta obtenida:%d",restap);
   
   
    }	
	
	return 0;
	
}