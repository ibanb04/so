#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>

int main (){

  int tb1[2],tb2[2],tb3[2],tb4[2];
  pipe(tb1);
  pipe(tb2);
  pipe(tb3);
  pipe(tb4);


  if(fork() == 0)
  {
      if(fork() == 0)
      {
        close(tb2[1]); close(tb1[0]);
        close(tb3[0]); close(tb4[1]);
        close(tb1[1]); close(tb4[0]);
        char t2[50];
        read(tb2[0],&t2,sizeof(t2));
        printf(" %s  el Proceso Hijo 2 [%d] \n",t2,getpid());
        write(tb3[1],&t2,sizeof(t2));



      }
      else
      {
        close(tb2[0]);
        close(tb3[1]);
        close(tb1[1]); close(tb4[0]);
        char t1[50],t3[50];

        read(tb1[0],&t1,sizeof(t1));
        printf(" %s  el Proceso Hijo 1 [%d] \n",t1,getpid());
        write(tb2[1],&t1,sizeof(t1));
        read(tb3[0],&t3,sizeof(t3));

        printf(" %s  el Proceso Hijo 1 [%d] \n",t3,getpid());
        write(tb4[1],&t3,sizeof(t3));


      }
  }
  else
  {
    close(tb2[0]); close(tb3[1]);
    close(tb2[1]); close(tb1[0]);
    close(tb3[0]); close(tb4[1]);

    char texto[50],t4[50];
    printf("Ingrese el texto a viajar por la tuberias\n" );
    fgets(texto,100, stdin);
    //strcpy(texto,"Soy el texto que Viaja Por");

    printf(" %s  el Proceso Padre  [%d] \n",texto,getpid());
    write(tb1[1],&texto,sizeof(texto));
    read(tb4[0],&t4,sizeof(t4));
    printf(" %s  el Proceso Padre  [%d] \n",t4,getpid());

  }
    return 0;
}
