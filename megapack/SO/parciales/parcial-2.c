#include <stdlib.h>
#include <string.h>
#include <sys/shm.h>
#include <stdio.h>
#include <unistd.h>
#include <wait.h>
#include <sys/ipc.h>
#include <signal.h>
#define N_HIJOS 5

typedef struct Mensaje // estructura del mensaje
{
	int pid_h; // pid de cada proceso
	int sw_msj; // un sw para controlar cada proceso
	int id;	    // el id para comparar con el que ingresa el usuario
	int tiempo; // tiempo que va de 0 a 3000000
	int off_on; // estado --> encendido apagado
	int off_on_ant;// se guarda el estado anterior para comparar si el que ingreso el user ya estaba
}Mensaje;

typedef struct Luz
{
	Mensaje mensaje[N_HIJOS]; // array de mensajes depende del numero de hijos
	int salir;	// variable para salir
}Luz;

void manejador(int signal){} // manejador de señales

void acciones(Luz *shm_luz, int i){ // funcion que realiza la accion que le corresponda
	
	if (shm_luz->mensaje[i].off_on != shm_luz->mensaje[i].off_on_ant ) // si no se encuentra en un estado que ya esta
	{
		if (shm_luz->mensaje[i].off_on == 1)
		{
			printf("[%d] La Luz numero %d se encendera en %d nanosegundos... \n",getpid(), i, shm_luz->mensaje[i].tiempo);
			usleep(shm_luz->mensaje[i].tiempo);
			printf("[%d] La Luz numero %d se acaba de encender...\n",getpid(), i);
		}else{
			printf("[%d] La Luz numero %d se apagara en %d nanosegundos... \n",getpid(), i, shm_luz->mensaje[i].tiempo);
			usleep(shm_luz->mensaje[i].tiempo);
			printf("[%d] La Luz numero %d se acaba de apagar...\n",getpid(), i);
		}
	}else{
		if (shm_luz->mensaje[i].off_on == 1)
		{
			printf("[%d] La Luz numero %d ya esta encendida \n",getpid(), i);
		}else{
			printf("[%d] La Luz numero %d ya esta apagada \n",getpid(), i);
		}
	}

	printf("\n");
}

int main(int argc, char const *argv[])
{
	signal(SIGUSR1, manejador); // se asigna la señal
	pid_t hijos[N_HIJOS];
	int padre=getpid();

	int shm_id;
	int shm_size = sizeof(Luz);
	Luz *shm_luz; // puntero con el que se va a trabajar

	shm_id = shmget(IPC_PRIVATE, shm_size, IPC_CREAT | 0600);
	shm_luz = shmat(shm_id, NULL, 0);

	shm_luz->salir = 0;

	int i=0;
	int id_luz=0;
	int time=0;
	int estado=0;
	
	//creamos los hijos 
	
	for(i=0;i<N_HIJOS;i++){
		hijos[i] = fork();
		if (hijos[i] == 0)
		{
			// a cada hijo lo inicializamos
			shm_luz->mensaje[i].pid_h = getpid();
			shm_luz->mensaje[i].sw_msj = 0;
			shm_luz->mensaje[i].id=i;
			shm_luz->mensaje[i].tiempo = 0;
			shm_luz->mensaje[i].off_on = -1;
			pause(); // lo pausamos a la espera de una señal del servidor
			
			//cuando se active el sw de cada hijo procedemos a realizar cada accion
			while(shm_luz->mensaje[i].sw_msj == 1){
				acciones(shm_luz,i); // realiza la accion 
				shm_luz->mensaje[i].sw_msj = 0; // se desactiva 
				kill(getppid(),SIGUSR1); // se le manda la señal al servidor para indica que ha terminado la accion
				pause(); // se pausa a la espera
			}
			printf("[%d] FINALIZANDO CLIENTE.......... \n", getpid());
			shmdt(shm_luz); 
			break;
		}
	}
	
	
	// servidor
	if (padre == getpid())
	{
		usleep(1000);

		do
		{
			printf("[%d] Ingrese el ID de la luz: ", getpid());
			scanf("%d", &id_luz);
			if (id_luz>=0 && id_luz<N_HIJOS)
			{
				printf("[%d] Ingrese el tiempo: ", getpid());
				scanf("%d", &time);	
				if (time>=0 && time <=3000000)
				{
					printf("[%d] Ingrese la operacion a realizar: (0)Apagar === (1)Encender: ", getpid());
					scanf("%d", &estado);
					if (estado>=0 && estado<=1)
					{
						// si todos los datos ingresados son validos se procede a lo siguiente
						shm_luz->mensaje[id_luz].tiempo = time;
						shm_luz->mensaje[id_luz].off_on_ant = shm_luz->mensaje[id_luz].off_on;
						shm_luz->mensaje[id_luz].off_on = estado;
						shm_luz->mensaje[id_luz].sw_msj = 1;
						// se le manda la señal al cliente correspondiente para que realice la accion
						kill(hijos[id_luz], SIGUSR1);
						pause(); // se pausa
					}else{
						printf("[%d] Error...!\n", getpid());
						printf("[%d] Operacion invalida\n", getpid());	
					}
				}else{
					printf("[%d] Error...!\n", getpid());
					printf("[%d] Tiempo invalido\n", getpid());
				}		
			}else{
				printf("[%d] Error...!\n", getpid());
				printf("[%d] ID invalido\n", getpid());
			}

			printf("[%d] Ingrese la tecla 1 para salir: ", getpid());
			scanf("%d", &shm_luz->salir);
			printf("\n");
		} while (shm_luz->salir!=1);

		// desactivamos a los hijos para que puedan terminar
		for(i=0;i<N_HIJOS;i++){
			shm_luz->mensaje[i].sw_msj = -1;
			kill(hijos[i], SIGUSR1);
			wait(NULL);
		}

		printf("[%d] FINALIZANDO SERVER.......... \n", getpid());
		shmdt(shm_luz);
		shmctl(shm_id,IPC_RMID,0);


	}

	return 0;
}
