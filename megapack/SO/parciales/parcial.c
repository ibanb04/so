#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

struct DatoHilo{int dato;};

void * funhilos(void *);

int posicionVector =0;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  cond  = PTHREAD_COND_INITIALIZER;

int main (){
//Leer Archivo
	int array[1024];
	int tam = 0;
	int numHilos = 0;

	FILE *file = fopen("data.txt", "r");
	 	if(file){
	 		int n=0;
	 		while(!feof(file)){
	 			int num = 0.0;
	 			fscanf(file,"%d", &num);
	 			array[n] = num;
	 			printf("%d\n",array[n]);
	 			n++;
	 		}
	 		numHilos= n;
	 		tam = n-1;

	 		fclose(file);
	 	}

	printf("este es el vector\n");  
	for (int i = 0; i < 5; i++)
	{	
		printf("%d\n",array[i]);
	}

	printf("el Numero de hilo es %d\n", numHilos);

	
// Creando hilos

	int i=0;
	pthread_t *pidhilos = NULL;
	struct DatoHilo *datohilo;
	pidhilos = (pthread_t *) calloc(numHilos, sizeof(pthread_t));


	for(i=0; i<numHilos; i++){	

	    datohilo = (struct DatoHilo*)malloc(sizeof(struct DatoHilo));
		datohilo->dato = array[i];
		pthread_create(&pidhilos[i], NULL, funhilos, (void*)datohilo);
		
	}
	
	for(i=0; i<numHilos; i++){

		pthread_join(pidhilos[i], NULL);
	}
    
	free(pidhilos);
	return 0;	
}

void * funhilos(void *arg){

	int valor = ((struct DatoHilo *)arg)->dato;
	
	pthread_mutex_lock(&mutex);

	while(posicionVector != valor) {
		pthread_cond_wait(&cond, &mutex);
	}	

	printf("Hilo valor %d\t[%u]\n", valor, (unsigned int)pthread_self());

	posicionVector ++;

	pthread_cond_broadcast(&cond);
	pthread_mutex_unlock(&mutex);
    
	free(arg);
	pthread_exit(0);

}