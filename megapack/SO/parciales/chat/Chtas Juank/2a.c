#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/shm.h>

#define SIZE 50

void Error(char *msj){
printf("%s\n",msj);
exit(1);
} 

int main (void){

int shmid1,shmid2,pidhijo;

char *shm_pointer1, *shm_pointer2,buff1[SIZE],buff2[SIZE];

int key1=ftok(".",2016);
int key2=ftok(".",2017);

shmid1=shmget(key1,SIZE,IPC_CREAT|0666);
if(shmid1==-1)Error("error creando shmget");

shmid2=shmget(key2,SIZE,IPC_CREAT|0666);
if(shmid2==-1)Error("error creando shmget");

shm_pointer1=shmat(shmid1,NULL,0);
if(shm_pointer1==(void*)-1)Error("adjuntando segmento shmat");
buff1[0]='\0';

shm_pointer2=shmat(shmid2,NULL,0);
if(shm_pointer2==(void*)-1)Error("adjuntando segmento shmat");
buff2[0]='\0';

if(fork()==0){

do {
        if (strcmp(buff2, shm_pointer2)) {
            strcpy(buff2, shm_pointer2);
            printf("%s\n", buff2);
        }
    } while(strcmp(shm_pointer2, "salir"));

}else{


do{

scanf("%s",buff1);
strcpy(shm_pointer1,buff1);
printf("%s\n",buff1);
}while(strcmp(buff1, "salir"));

shmctl(shmid1, IPC_RMID, NULL);
    printf("Finalizando... [%d]\n", getpid());


}
    return 0;

}
