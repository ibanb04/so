#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#define max 100

int main(){
	int tb1[2], tb2[2];
	pipe (tb1);
	pipe (tb2);

	if (fork()==0){
		close (tb1[0]);
		close (tb2[1]);
		int tam=0,cont=0,i;
	
	read (tb2[0], &tam, sizeof(tam));	
	char vec[tam];
	read (tb2[0], &vec, sizeof(vec));
	for(i=0;i<tam;i++){
		if(vec[i]=='a' || vec[i]=='e' || vec[i]=='i' || vec[i]=='o' || vec[i]=='u'){
		cont++;			
		}
	}
	printf ("\nel numero de vocales son [%d]\n", cont);
	write (tb1[1],&tam,sizeof(tam));
	write (tb1[1],&vec,sizeof(vec));


}
	else if (fork()==0){
		close (tb1[0]);
		close (tb2[0]);	
		close (tb1[1]);
		char txt[max];
		int i=0;
		FILE *archivo;
		
		archivo = fopen ("vocales.txt","r");

		printf ("el contenido del archivo es \n");
		while (feof (archivo)==0){
			txt[i]=fgetc(archivo);
			i++;
		
		}
		i=i-1;


		write (tb2[1],&i,sizeof(i));
	
		write (tb2[1],&txt,sizeof(txt));
	
		
		fclose(archivo);
		
		}

	else {
		close (tb1[1]);
		close (tb2[0]);
		close (tb2[1]);
		int tam=0,i=0,cont=0;	
		read (tb1[0], &tam, sizeof(tam));	
		char vec[tam];
		read (tb1[0], &vec, sizeof(vec));
		for(i=0;i<tam;i++){
		if(vec[i]=='\n'){
		cont++;			
		}
	
	}
		
printf ("\nel numero de saltos de linea  son [%d]\n", cont);	
}
	return 0;
}

