#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <stdlib.h>

void *manejaHilo(void *);


typedef struct Nodo
{
	double vec[10];
	int size;
}Nodo;

double promedio(Nodo*);

int main(int argc, char const *argv[])
{
	printf("Hola\n");
	pthread_t pidHilo;
	Nodo *nodo;
	nodo->size = 10;
	
	for (int i = 0; i < 10; ++i)
	{
		nodo->vec[i] = i*2;
	}

	
		
		pthread_create(&pidHilo, NULL, manejaHilo, nodo);
		
	
		printf("Hilo principal (id->%ld)\n", (long int) pthread_self());
	
		pthread_join(pidHilo,NULL);
	
	
	return 0;
}

void *manejaHilo(void *param){
	double prom = promedio((Nodo *)param);
	printf("Hilo de ejecución (id->%ld), promedio: %f\n", (long int) pthread_self(), prom);
	pthread_exit(0);
}

double promedio(Nodo *nodo){
	double prom = 0.0, suma=0.0;
	int tam = nodo->size;

	for (int i = 0; i < tam; ++i)
	{
		suma+=nodo->vec[i];
	}
	prom = suma/tam;

	return prom;
}