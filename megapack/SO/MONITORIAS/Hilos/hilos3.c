#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

void *manejaHilo(void *);

typedef struct Nodo
{
	int val;
}Nodo;

int main(int argc, char const *argv[])
{
	pthread_t pidHilo;
	Nodo nodo;
	nodo.val = 5;
	pthread_create(&pidHilo, NULL, manejaHilo, (void*) &nodo);
	printf("Hilo principal (id->%ld)\n", (long int) pthread_self());
	pthread_join(pidHilo,NULL);
	return 0;
}

void *manejaHilo(void *param){
	printf("Hilo de ejecución (id->%ld), param: %d\n", (long int) pthread_self(), ((Nodo*)param)->val);
	pthread_exit(0);
}