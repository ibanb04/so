#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

void *manejaHilo(void *);

typedef struct Nodo
{
	int val;
}Nodo;

int main(int argc, char const *argv[])
{
	pthread_t pidHilo[3];
	Nodo nodo;
	

	for (int i = 0; i < 3; ++i)
	{
		nodo.val = i;
		pthread_create(&pidHilo[i], NULL, manejaHilo, (void*) &nodo);
	}
	
	printf("Hilo principal (id->%ld)\n", (long int) pthread_self());
	for (int i = 0; i < 3; ++i)
	{
		pthread_join(pidHilo[i],NULL);
	}
	
	return 0;
}

void *manejaHilo(void *param){
	printf("Hilo de ejecución (id->%ld), param: %d\n", (long int) pthread_self(), ((Nodo*)param)->val);
	pthread_exit(0);
}