
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>	



void* funcion_maneja_hilo(void *);

int *vector, SUMA;
int n=0;
typedef struct Nodo{
	int posiciones;
	int lini;
	int lfin;
	
}Nodo;

pthread_mutex_t mutex;
pthread_barrier_t barrier;

int main(int argc, char const *argv[])
{
	SUMA = 0;
	
	int num_hilos = 0;
	int nip = 0;

	printf("Ingrese el numero de posiciones del vector: ");
	scanf("%d",&n);	
	
	printf("Ingrese el numero de hilos a procesar: ");
	scanf("%d",&num_hilos);
	

	pthread_t pidhilos[num_hilos];
	pthread_barrier_init(&barrier,NULL, num_hilos);

	Nodo *nodo[num_hilos];
	nip = n/num_hilos;

	vector = (int *) calloc (n, sizeof(int));

	for(int i=0; i<n;i++){
		vector[i] = i;			
	}
	
	

	for (int i = 0; i < num_hilos; i++)
	{
		/* code */
		nodo[i] = (Nodo *) malloc(sizeof(Nodo));
		nodo[i]->lini = i*nip;
		nodo[i]->lfin = (nodo[i]->lini+nip)-1;
		nodo[i]->posiciones = i;
		pthread_create(&pidhilos[i],NULL,funcion_maneja_hilo,nodo[i]);
	}

	for (int i = 0; i < num_hilos; i++)
	{
		pthread_join(pidhilos[i],NULL);
	}

	printf("Hilo pricipal\n");
	printf("Suma: %d\n", SUMA);

	return 0;
}


void* funcion_maneja_hilo(void *param){
	int suma = 0;	
	int ret = 0;
	int ini = ((( Nodo *) param)->lini);
	int fin = (((Nodo *) param)->lfin);
	int pos = ((( Nodo *) param)->posiciones);
	double promedio = 0.0;

	for (int i=ini; i<=fin; i++){
		suma = suma + vector[i];
	}

	pthread_mutex_lock(&mutex);
	SUMA+=suma;
	pthread_mutex_unlock(&mutex);

	ret = pthread_barrier_wait(&barrier);

	if (ret == PTHREAD_BARRIER_SERIAL_THREAD)
	{
		promedio = SUMA/(double)n;
		printf("Suma %d\n", SUMA);
		printf("promedio: %f\n", promedio);
	}

	pthread_exit(NULL);
}