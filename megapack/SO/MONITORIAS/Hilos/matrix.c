#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>	
#include <time.h>
#define NUM_HIJOS 3

void* funcion_maneja_hilo(void *);
int **matrix_A, **matrix_B, **matrix_C;

void inicializar_matrix(int, int);
void llenar_matrix(int, int);
void imprimir_matrix(int **, int, int);
void multiplicar_matrix_diagonal(int, int);
void multiplicar_matrix_superior(int, int);
void multiplicar_matrix_inferior(int, int);

typedef struct Nodo
{
	int filas;
	int columnas;
	int id;
}Nodo;

int main(int argc, char const *argv[])
{
	srand(time(NULL));
	int filas=0, columnas=0;

	printf("Ingrese el numero de Filas de la matriz: ");
	scanf("%d", &filas);

	printf("Ingrese el numero de Columnas de la matriz: ");
	scanf("%d", &columnas);

	pthread_t pidhijos[NUM_HIJOS];

	inicializar_matrix(filas, columnas);
	llenar_matrix(filas, columnas);
	//imprimir_matrix(matrix_A, filas, columnas);

	Nodo *nodo[NUM_HIJOS];

	for (int i = 0; i < NUM_HIJOS; ++i)
	{
		nodo[i] = (Nodo *) malloc(sizeof(Nodo));
		nodo[i] -> filas = filas;
		nodo[i] -> columnas = columnas;
		nodo[i] -> id = i;
		pthread_create(&pidhijos[i],NULL, funcion_maneja_hilo, nodo[i]);
	}

	for (int i = 0; i < NUM_HIJOS; ++i)
	{
		pthread_join(pidhijos[i],NULL);
	}

	imprimir_matrix(matrix_A, filas, columnas);
	printf("\n");
	imprimir_matrix(matrix_B, filas, columnas);
	printf("\n");
	imprimir_matrix(matrix_C, filas, columnas);
	return 0;
}


void inicializar_matrix(int filas, int columnas){
	int i = 0;
	matrix_A = (int **) calloc(filas,sizeof(int*));
	matrix_B = (int **) calloc(filas,sizeof(int*));
	matrix_C = (int **) calloc(filas,sizeof(int*));
	for(i=0;i<filas;i++){
		matrix_A[i] = (int *) calloc(columnas,sizeof(int));
		matrix_B[i] = (int *) calloc(columnas,sizeof(int));
		matrix_C[i] = (int *) calloc(columnas,sizeof(int));
	}
}

void llenar_matrix(int filas, int columnas){
	for (int i = 0; i < filas; ++i)
	{
		for (int j = 0; j < columnas; ++j)
		{
			matrix_A[i][j] = rand()%5;
			matrix_B[i][j] = rand()%5;
			matrix_C[i][j] = 0;
		}
	}
}

void* funcion_maneja_hilo(void *param){

	int filas = ((Nodo *) param)->filas;
	int columnas = ((Nodo *) param)->columnas;
	int id = ((Nodo *) param)->id;

	switch(id){
				case 0:
						multiplicar_matrix_inferior(filas, columnas);
						break;
				case 1:
						multiplicar_matrix_diagonal(filas, columnas);
						
						break;
				case 2: 
						multiplicar_matrix_superior(filas, columnas);
						
						break;
			}
	
	pthread_exit(NULL);
}

void imprimir_matrix(int **matrix, int filas, int columnas){
	for (int i = 0; i < filas; ++i)
	{
		printf("| ");
		for (int j = 0; j < columnas; ++j)
		{
			printf("%d  ", matrix[i][j]);
		}
		printf("|\n\n");
	}
}

void multiplicar_matrix_superior(int filas, int columnas){
	int i=0, j=0, k=0;
	for ( i = 0; i < filas; i++)
	{
		for ( j = columnas-1; j > i; j--)
		{
			for (k = 0; k < columnas; k++)
			{
				matrix_C[i][j]+=matrix_A[i][k]*matrix_B[k][j];
			}

		}
	}
}

void multiplicar_matrix_diagonal(int filas, int columnas){
	int i=0, k=0;
	for ( i = 0; i < filas; i++)
	{
			for (k = 0; k < columnas; k++)
			{
				matrix_C[i][i]+=matrix_A[i][k]*matrix_B[k][i];
			}
	
	}
}

void multiplicar_matrix_inferior(int filas, int columnas){
	int i=0, j=0, k=0;
	for ( i = 1; i < filas; i++)
	{
		for ( j = 0; j < i; j++)
		{
			for (k = 0; k < columnas; k++)
			{
				matrix_C[i][j]+=matrix_A[i][k]*matrix_B[k][j];
			}
		}
	}
}
