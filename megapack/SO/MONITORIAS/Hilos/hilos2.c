#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

void *manejaHilo(void *);

int main(int argc, char const *argv[])
{
	pthread_t pidHilo;
	pthread_create(&pidHilo, NULL, manejaHilo, (void*) "Hola Mundo");
	printf("Hilo principal (id->%ld)\n", (long int) pthread_self());
	pthread_join(pidHilo,NULL);
	return 0;
}

void *manejaHilo(void *param){
	printf("Hilo de ejecución (id->%ld), param: %s\n", (long int) pthread_self(), (char*)param);
	pthread_exit(0);
}