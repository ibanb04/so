#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

void* funcion_maneja_hilo(void *);

int main(){
	pthread_t pidhilo;
	pthread_create(&pidhilo, NULL, funcion_maneja_hilo, NULL);
	printf("Hilo principal (idthread -> [%ld])\n", (long int) pthread_self());
	pthread_join(pidhilo, NULL); //<- si se elimina, dos salidas, una solo el hilo princ, dos que el hilo princ se bloquee dejando ejecutar al hijo
	return 0;
}

void * funcion_maneja_hilo(void*param){
	printf("Hilo (idthread -> [%ld])\n", (long int) pthread_self());
	pthread_exit(0);
}

/*

equivale a

if fork == 0

	print hijo

else
	print padre


 */
