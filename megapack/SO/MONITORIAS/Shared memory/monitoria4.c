#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/shm.h>
#include <unistd.h>
#include <wait.h>

int main(int argc, char *argv[]) {

    pid_t pidhijo;

    int shmid = shmget(IPC_PRIVATE, 50, IPC_CREAT|0666);
    char *shm_ptr = shmat(shmid, NULL, 0);
    
    pidhijo = fork();

    if (pidhijo == 0) {
        usleep(10000);
        printf("[%d] Leido: %s\n", getpid(), shm_ptr);
        shmdt(shm_ptr);
    } else {
        strcpy(shm_ptr, "Hola memoria compartida");
        printf("[%d] Escrito: Hola memoria compartida\n", getpid());
        wait(NULL);
        shmdt(shm_ptr);
        shmctl(shmid, IPC_RMID, NULL);
    }

    return 0;
}
