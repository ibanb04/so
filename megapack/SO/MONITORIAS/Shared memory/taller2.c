#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/shm.h>
#include <unistd.h>
#include <wait.h>

#define MAX_SIZE 100000000


unsigned int sizeof_dm(int rows, int col, size_t sizeElement){

size_t size = rows * sizeof(double *);
size += (col*sizeElement);
return size;	

}

void create_index(void **m, int rows, int cols, size_t sizeElement){
	int i=0;
	size_t sizeRow = cols * rows * sizeElement;
	m[0] = m+rows;
	for (i = 1; i < rows; ++i)
	{
		m[i] = (m[i-1] + sizeRow);
	}
}

void llenarMatrix(int rows, int cols, double **m){
	int i=0, j=0, count = -1;
	for (i = 0; i < rows; ++i)
	{
		for (j = 0; j < cols; ++j)
		{
			m[i][j] = ++count;
		}
	}
}

void printMatrix(int rows, int cols, double **m){
	for (int x = 0; x < rows; ++x)
		{
			for (int y = 0; y < cols; ++y)
			{
				printf("%.2f\t", m[x][y]); 
			}
			printf("\n");
		}
}

int main(int argc, char const *argv[])
{
	
	int rows=0;
	int cols=0;
    double *psuma;
    int suma=0,val;
    int shm_suma;
    int shm_size=MAX_SIZE*sizeof(double);

    shm_suma=shmget(IPC_PRIVATE,shm_size,IPC_CREAT|S_IRUSR|S_IWUSR);
    psuma=shmat(shm_suma,0,0);

	printf("IGRESE EL NUMERO DE FILAS: ");
	scanf("%d", &rows);

	printf("IGRESE EL NUMERO DE COLUMNAS: ");
	scanf("%d", &cols);

	pid_t hijo;

	size_t sizeMatrix = sizeof_dm(rows,cols,sizeof(double));
	int shmid = shmget(IPC_PRIVATE, sizeMatrix, IPC_CREAT|0666);
	double **matrix = shmat(shmid, NULL, 0);
	create_index((void*)matrix, rows, cols, sizeof(double));
    for(int i=0;i<cols;i++){
     hijo=fork();
     if(hijo==0){
       for(int j=0;j<cols;j++){
          val=m[i][j];
          psuma=psuma+val;
       }
      break;}

}

	hijo = fork();

	if (hijo!=0) //PADRE
	{
		llenarMatrix(rows, cols, matrix);
		wait(NULL);
		printf("\n");
		printMatrix(rows, cols, matrix);
		shmdt(matrix);
		shmctl(shmid, IPC_RMID, NULL);
	}else{//////// HIJO
		usleep(1000);
		printf("Desde El hijo\n\n");
		printMatrix(rows, cols, matrix);
		matrix[2][2] = -99.0;
		shmdt(matrix);
	}

	return 0;
}
