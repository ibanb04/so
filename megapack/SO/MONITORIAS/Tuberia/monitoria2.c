#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wait.h>

double calcular_promedio(int tam, int vector[]){
	double promedio = 0.0;
	int sum = 0;

	for (int i = 0; i < tam; i++)
	{
			
		sum+= vector[i];
	}	

	promedio = sum/tam;

	return promedio;
}

int main(int argc, char const *argv[])
{
	
	int fdph[2];//tuberia de padre a hijo
	int fdhp[2];//tuberia de hijo a padre
	int array[1024];
	int arrayHijo[1024];
	pipe(fdph);
	pipe(fdhp);

	int tamVector =0;

	pid_t hijo = fork();

	if (hijo==0)//hijo
	{
		close(fdph[1]);
		close(fdhp[0]);
		int leer;
		double sum = 0.0;
		read(fdph[0], &leer, sizeof(int));
		printf("tamaño : %d \n", leer);

		for (int i = 0; i < leer; i++)
		{
			read(fdph[0], &arrayHijo[i], sizeof(int));
			//sum+= arrayHijo[i];
		}
		//sum=sum/leer;
		//printf("promedio: %f\n", sum);
		sum = calcular_promedio(leer, arrayHijo);
		write(fdhp[1], &sum, sizeof(double));



	}else{//padre
		FILE *file = fopen("data.txt","r");
		if (file) {
        	int tam = 0;
        	while(!feof(file)) {
	            int num = 0.0;
	            fscanf(file, "%d", &num);
	            array[tam] = num;
	            
	            tam++;
        	}
        	tam--;
        	fclose(file);

        	//imprimir vector archivo

			for (int i = 0; i < tam; ++i)
			{
				printf("%d\n", array[i]);
			}
			tamVector = tam;
		}

		close(fdph[0]);
		close(fdhp[1]);
		write(fdph[1], &tamVector, sizeof(int));

		for (int i = 0; i < tamVector; i++)
		{
			write(fdph[1], &array[i], sizeof(int));
		}
		wait(NULL);

		double result;
		read(fdhp[0], &result, sizeof(double));
		printf("promedio: %f\n", result);
	}
	return 0;
}