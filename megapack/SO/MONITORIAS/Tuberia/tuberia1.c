#include <stdio.h>
#include <unistd.h>
#include <wait.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>

int main(int argc, char const *argv[])
{
	
	int fd[2];
	int n=0;

	char entrada[1024];
	char salida[1024];

	pipe(fd);
	strcpy(entrada, "Hola mundo tuberias...\n");
	
	write(fd[1], entrada, strlen(entrada));

	n = read(fd[0], salida, 1024);

	salida[n] = '\0';

	printf("Leido: %s \n", salida);

	return 0;
}