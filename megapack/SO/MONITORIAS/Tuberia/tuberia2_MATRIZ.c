#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/shm.h>
#include <unistd.h>
#include <wait.h>


unsigned int sizeof_dm(int rows, int col, size_t sizeElement){

size_t size = rows * sizeof(double *);
size += (col*sizeElement);
return size;	

}

void create_index(void **m, int rows, int cols, size_t sizeElement){
	int i=0;
	size_t sizeRow = cols * rows * sizeElement;
	m[0] = m+rows;
	for (i = 1; i < rows; ++i)
	{
		m[i] = (m[i-1] + sizeRow);
	}
}

void llenarMatrix(int rows, int cols, double **m){
	int i=0, j=0, count = -1;
	for (i = 0; i < rows; ++i)
	{
		for (j = 0; j < cols; ++j)
		{
			m[i][j] = ++count;
		}
	}
}

void printMatrix(int rows, int cols, double **m){
	for (int x = 0; x < rows; ++x)
		{
			for (int y = 0; y < cols; ++y)
			{
				printf("%.2f\t", m[x][y]); 
			}
			printf("\n");
		}
}

int main(int argc, char const *argv[])
{
	
