#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <wait.h>
#include <time.h>
#define MAX_SIZE 10000


struct timespec tic, toc;
double Elapsed;



double calculo (double sumatoria[], int indice){
	double valReturn=0.0;
	double acomulador=0.0;
	int i =0;
	for (i = 0; i <indice ; i++)
	{
		//printf("valor V[%d]=%f\n",i,sumatoria[i]);
		acomulador += sumatoria[i];
	}
	valReturn = acomulador/MAX_SIZE;
	return valReturn;
}


void cerrar(int id,int fd[][2]){
	int i=0;
	
	for(i=0;i<=id;i++){
		close(fd[i][0]);
		if(id!=i){
			close(fd[i][1]);
		}
		
	}

}

int main(int argc, char const *argv[])
{	
	


	//int padre = getpid();
	
	int padre = getpid();
	int n, npro=0;
	
	printf("Ingrese la cantidad de procesos a ejecutar: ");
    scanf("%d",&npro);
    
     a =(double*) calloc(MAX_SIZE,sizeof(double));
     b =(double*) calloc(MAX_SIZE,sizeof(double));
    double arrayv[npro];
    double suma = 0.0;

    pid_t hijos[npro];
    int fdHP[npro][2];
	
	


	
	
	clock_gettime(CLOCK_REALTIME, &tic);
	for (int i = 0; i < npro; i++)
	{	

		pipe(fdHP[i]);
		double val =0.0;
		double aco = 0.0;

		hijos[i] = fork();

		
		if (hijos[i] == 0) // hijos
		{
			
			break;
		}
	}
		
	
	if(padre==getpid()){
		for (int i = 0; i < npro; i++)
		{
			/* code */
			close(fdHP[i][1]);
			read(fdHP[i][0], &suma, sizeof(double));
			arrayv[i] = suma;
			wait(NULL);
		}

		printf("La suma total es: %.35f\n",  calculo(arrayv,npro));
		clock_gettime(CLOCK_REALTIME, &toc);
		Elapsed = (toc.tv_sec-tic.tv_sec)+((toc.tv_nsec-tic.tv_nsec)/(double)1E9);
		printf("Tiempo realizando el procedimiento: %.15f\n\n", Elapsed );
	}

		//borramos la memoria
		free(a);
		free(b);

		
		
	return 0;
}