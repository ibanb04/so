#include <stdio.h>
#include <unistd.h>
#include <wait.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#define NPRO 3

int read_file(const char *filename, double array[]) {
    FILE *file = fopen(filename, "r");

    if (file) {
        int n = 0;
        while(!feof(file)) {
            int num = 0.0;
            fscanf(file, "%d", &num);
            array[n++] = num;
        }

        fclose(file);

        return n - 1;
    }

    return 0;
}


int main(int argc, char const *argv[])
{
	int fdPH[NPRO][2];
	int fdHP[NPRO][2];
	pid_t hijos[NPRO];
	
	int array[1024];
	int array_leido[1024];
	int tam  = 0, sum=0;
	double resultado = 0.0;
	int i = 0;

	for (i = 0; i < NPRO; i++)
	{	
		pipe(fdHP[i]);
		pipe(fdPH[i]);
		int tmp=0;
		hijos[i] = fork();
		
		if (hijos[i]==0)
		{
			

			
					if(i==1){
						read(fdPH[i][0], &tmp, sizeof(int));
						for (int x = 0; x < tmp; x++)
						{
							read(fdPH[i][0], &array_leido[x], sizeof(int));
							sum += array_leido[x];
						}
						double prom = sum/tmp;

						write(fdHP[1][1], &prom, sizeof(double));
					}
									
				break;
			}
			
		}
		
	

	if (i==NPRO)
	{	
		FILE *file = fopen("data.txt", "r");
		
	    	if (file) {
	        	int n = 0;
	        	while(!feof(file)) {
		            int num = 0.0;
		            fscanf(file, "%d", &num);
		            array[n] = num;
		            //printf("%d\n", array[n]);
		            n++;
	        	}
	        	tam = n-1;
	        	fclose(file);
	        }

	      
	        for (int j = 0; j < NPRO; j++)
	        {
	        	 write(fdPH[j][1], &tam, sizeof(int));
	        	 for (int x = 0; x < tam; x++)
				 {
					write(fdPH[0][1], &array[x], sizeof(int));
				 }
				 wait(NULL);
				 
				 printf("llego\n");
				 read(fdHP[1][0], &resultado, sizeof(double));
				 printf("%f\n", resultado );

	        	
	        }

	}


	return 0;
}