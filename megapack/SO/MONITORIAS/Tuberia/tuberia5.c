#include <stdio.h>
#include <unistd.h>
#include <string.h>
//#include sys/types.h
#include <wait.h>


#define MAX_READ 1024
char buff[MAX_READ];
#define EOL '\0'

int main(){
int fd[2], n;
pipe(fd);
if(fork()!=0){
   close(fd[0]);
    do{
      fflush(stdin);
      buff[0]=EOL;
      gets(buff);
      printf("escritorio: %s\n",buff);
      write(fd[1], buff,strlen(buff));
    }while(strcmp(buff,"quit") !=0);
        close(fd[1]);
        wait(NULL);
    }else{
       close(fd[1]);
       while((n=read(fd[0],buff,MAX_READ))> 0){
           buff[n] = EOL;
           printf("leido: %s\n",buff);
               if(strcmp(buff,"quit")==0) break;
               
               } 
    
    }

return 0;


}