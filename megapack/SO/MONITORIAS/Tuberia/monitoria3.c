#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wait.h>
#define npro 3

int main(int argc, char const *argv[])
{
	int fdph[npro][2];
	int fdhp[npro][2];
	int tamVector = 0;
	int array[1024];
	int arrayHijo[1024];
	int leerTam = 0;
	double suma = 0.0;
	double prom = 0.0;
	int numPares = 0;
	int numImpares = 0;
	double promedio=0.0;
	int par = 0;
	int impar = 0;
	//crear tuberias y cerrar las que no utiliza

	for (int i = 0; i < npro; i++)
	{
		pipe(fdph[i]);
		pipe(fdhp[i]);
	}

	pid_t hijos[npro];
	int i = 0;

	for ( i = 0; i < npro; i++)
	{
		hijos[i] = fork();

		if (hijos[i]==0)
		{
			///////////////cerrando tuberias/////
			//usleep(100);
			for (int x = 0; x < npro; x++)
			{
				close(fdph[x][1]);
				close(fdhp[x][0]);
				if (i!=x)////////////////////
				{////////////////////////////  cerrar tuberias que no
					close(fdph[x][0]);///////	le corresponden
					close(fdhp[x][1]);///////
				}////////////////////////////
			}
			/////////////////////////////
			
			read(fdph[i][0], &leerTam, sizeof(int));
			for (int x = 0; x < leerTam; x++)
			{
				read(fdph[i][0], &arrayHijo[x], sizeof(int));
			}

			switch(i){
				case 0:
					//calcular promedio
					printf("Hijo [%d]--->padre [%d] tamVector: %d \n", 
						getpid(),getppid(), leerTam);

					for (int x = 0; x < leerTam; x++)
					{
						suma+=arrayHijo[x];
					}

					prom = suma/leerTam;
					write(fdhp[i][1], &prom, sizeof(prom));
					//printf("Promedio=%f\n", prom);
					break;
				case 1:
				// calcular numeros pares
				printf("Hijo [%d]--->padre [%d] tamVector: %d \n", 
						getpid(),getppid(), leerTam);

					for (int x = 0; x < leerTam; x++)
					{
						if (arrayHijo[x]%2==0)
						{
							numPares++;
						}
					}
					write(fdhp[i][1], &numPares, sizeof(numPares));
					//printf("Numeros pares: %d\n", numPares);
					break;
				case 2:
				printf("Hijo [%d]--->padre [%d] tamVector: %d \n", 
						getpid(),getppid(), leerTam);
				for (int x = 0; x < leerTam; x++)
					{
						if (arrayHijo[x]%2!=0)
						{
							numImpares++;
						}
					}
					write(fdhp[i][1], &numImpares, sizeof(numImpares));
				//printf("Numeros impares: %d\n", numImpares);
					break;
			}
			

			break;
		}
	}

	if (i == npro)
	{	
		///////////////// leer archivo
		FILE *file = fopen("data.txt","r");
		if (file) {
        	int tam = 0;
        	while(!feof(file)) {
	            int num = 0.0;
	            fscanf(file, "%d", &num);
	            array[tam] = num;
	            
	            tam++;
        	}
        	tam--;
        	fclose(file);

        	//imprimir vector archivo

			for (int i = 0; i < tam; ++i)
			{
				//printf("%d\n", array[i]);
			}
			tamVector = tam;
		}
		////////////////////// //////////////

		/// esperar hijos
		for (int x = 0; x < npro; x++)
		{
			close(fdph[x][0]);
			close(fdhp[x][1]);
			write(fdph[x][1], &tamVector, sizeof(tamVector));
			
			for (int i = 0; i < tamVector; i++)
			{
				write(fdph[x][1], &array[i], sizeof(int));
			}

			wait(NULL);


		}

		printf("padre [%d] \n", getpid());

		read(fdhp[0][0], &promedio, sizeof(double));
		printf("Promedio: %f\n", promedio);
		read(fdhp[1][0], &par, sizeof(int));
		printf("Pares: %d\n", par);
		read(fdhp[2][0], &impar, sizeof(int));
		printf("Pares: %d\n", impar);
	}
	return 0;
}