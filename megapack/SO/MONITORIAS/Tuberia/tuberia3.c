#include <stdio.h>
#include <unistd.h>
#include <wait.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>

//dialogo simple entre padre e hijo

int main(int argc, char const *argv[])
{
	
	int fdPH[2], fdHP[2], n;
	char entrada[1024];
	char salida[1024];

	//creamos las tuberias

	pipe(fdPH);
	pipe(fdHP);

	pid_t pidhijo = fork();

	if (pidhijo!=0)//padre
	{
		close(fdPH[0]);
		close(fdHP[1]);

		strcpy(entrada,"Hola hijo como estas?...");
		write(fdPH[1], entrada, strlen(entrada));

		printf("Escribio [%d]: %s\n", getpid(), entrada );
		close(fdPH[1]);
		wait(NULL);
		n = read(fdHP[0], salida, 1024);
		salida[n] = '\0';
		printf("Leido [%d]: %s\n\n",getpid() ,salida);
		close(fdHP[0]);


	}else{//hijo

		usleep(100);
		close(fdPH[1]);
		close(fdHP[0]);

		n = read(fdPH[0], salida, 1024);
		salida[n] = '\0';
		printf("Leido [%d]: %s\n\n",getpid() ,salida);
		close(fdPH[0]);

		strcpy(entrada,"Muy bien padre...");
		write(fdHP[1], entrada, strlen(entrada));
		printf("Escribio [%d]: %s\n", getpid(), entrada );
		close(fdHP[1]);



	}


	return 0;
}